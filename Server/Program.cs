﻿using System;

namespace Server
{
    /// <summary>
    /// Hlavní třída serverové aplikace.
    /// </summary>
    class MainClass
    {
        /// <summary>
        /// Vstupní bod programu. Načte konfigurační soubor a vytvoří databázi (z určené zálohy, z výchozí zálohy, ze zálohy a žurnálu či přázdnou databázi).
        /// Dále vytvoří třídu spojení <see cref="ServerProcess"/> a třídu <see cref="ServerApp"/>, která řídí chod serverové aplikace.
        /// </summary>
        /// <param name="args">Parametry z příkazové řádky.</param>
        public static void Main(string[] args) {

            bool restore = false;
            string executable = System.Reflection.Assembly.GetEntryAssembly().Location;
            string path;
            if(IsLinux) {
                path = executable.Substring(0, executable.LastIndexOf('/')) + '/';
            }
            else {
                path = executable.Substring(0, executable.LastIndexOf('\\')) + '\\';
            }
            string conf = path + "brs_server.conf";

            Console.WriteLine("Bus Reservation System 1.0 - Server\nPetr Stefan - Programming in C#, 2014/2015\n");

            MemoryDb db;
            //bool result = true;

            string dataDir;
            int port;
            if(!ReadConf(conf, out port, out dataDir)) {
                Console.WriteLine("Processing config file 'brs_server.conf' failed. ABORTING.");
                return;
            }

            if(args.Length > 0) { // obnova dat z určené zálohy
                System.IO.FileStream stream;
                try {
                    stream = new System.IO.FileStream(args[0], System.IO.FileMode.Open);
                }
                catch(Exception) {
                    stream = null;
                }
                if(stream != null) {
                    db = new MemoryDb(stream);
                    stream.Close();
                    Console.WriteLine("Database restored.");
                    restore = true;
                }
                else {
                    db = new MemoryDb();
                    Console.WriteLine("Restore database failed. Creating empty one.");
                }
            }
            else { // načtení posledně platné databáze
                System.IO.FileStream stream;
                try {
                    stream = new System.IO.FileStream(dataDir + "brs.database", System.IO.FileMode.Open);
                }
                catch(Exception) {
                    stream = null;
                }
                if(stream != null) {
                    db = new MemoryDb(stream);
                    stream.Close();
                    Console.WriteLine("Database loaded.");
                    restore = true;
                }
                else {
                    db = new MemoryDb();
                }
                // zpracování urnálu (pokud existuje)
                ReadJournal journal = new ReadJournal(dataDir, db);
                uint journalItems;
                if(journal.Replay(out journalItems)) {
                    if(journalItems != 0) {
                        Console.WriteLine("Journal - {0} items processed.", journalItems);
                        restore = true;
                    }
                }
                else {
                    Console.WriteLine("Processing journal failed (at item {0})", journalItems);
                }
            }
            db.SetDataDir(dataDir);

            ServerProcess process = new ServerProcess(db, 4567);

            ServerApp terminal = new ServerApp(db, process);
            terminal.Run(restore);
            return;
        }
        private static bool ReadConf(string conf, out int port, out string dataDir) {
            bool result = true;
            System.IO.FileStream stream;
            try {
                stream = new System.IO.FileStream(conf, System.IO.FileMode.Open);
            }
            catch(Exception) {
                stream = null;
            }
            port = 0;
            dataDir = null;
            if(stream != null) {
                string line;
                System.IO.StreamReader reader = new System.IO.StreamReader(stream);
                while((line = reader.ReadLine()) != null && result) {
                    if(line == "") {
                        continue;
                    }
                    else if(line[0] == '#') {
                        continue;
                    }
                    else if(line.Contains("datadir")) {
                        string[] splited = line.Split('=');
                        if(splited.Length != 2 || splited[0] != "datadir") {
                            Console.WriteLine("brs_server.conf - wrong 'datadir' line.");
                            result = false;
                        }
                        else {
                            if(IsValidPath(splited[1])) {
                                dataDir = splited[1];
                            }
                            else {
                                result = false;
                            }
                        }
                    }
                    else if(line.Contains("port")) {
                        string[] splited = line.Split('=');
                        if(splited.Length != 2 || splited[0] != "port") {
                            Console.WriteLine("brs_server.conf - wrong 'port' line.");
                            result = false;
                        }
                        else {
                            int temp;
                            if(Int32.TryParse(splited[1], out temp)) {
                                port = temp;
                            }
                            else {
                                Console.WriteLine("brs_server.conf - 'port' - invalid number.");
                                result = false;
                            }
                        }
                    }
                    else {
                        Console.WriteLine("Wrong format of 'brs_server.conf' file.");
                        result = false;
                    }
                }
                reader.Close();
                if(port == 0 || dataDir == null) {
                    Console.WriteLine("brs_server.conf - not all required data provided.\n\tCheck documentation or example config file.");
                    return false;
                }
            }
            else {
                Console.WriteLine("Cannot open 'brs_server.conf' file.");
                result = false;
            }
            return result;
        }
        private static bool IsValidPath(string path) {
            string invalid = new string(System.IO.Path.GetInvalidPathChars());
            string regexString = "[" + System.Text.RegularExpressions.Regex.Escape(invalid) + "]";
            var containsBadCharacter = new System.Text.RegularExpressions.Regex(regexString);

            if(containsBadCharacter.IsMatch(path)) {
                Console.WriteLine("brs_server.conf - 'datadir' - invalid characters in path.");
                return false;
            }
            else if(!System.IO.Directory.Exists(path)) {
                Console.WriteLine("Provided directory does not exist.");
                return false;
            }
            return true;
        }
        private static bool IsLinux
        {
            get
            {
                int p = (int)Environment.OSVersion.Platform;
                return (p == 4) || (p == 6) || (p == 128);
            }
        }
    }
}
