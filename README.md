# BRS# 
Rezervační systém pro autobusy vznikl jako zápočtový projekt pro předměty **Jazyk C# a platforma .NET** a **Pokročilé programování pro .NET I** vyučovaných na MFF UK v akademickém roce 2014/2015. Projekt byl vyvíjen v prostředí *Mono* ve verzi 3.12.0 na operačním systému GNU/Linux. Podrobný popis architektury a funkcí je v obsažené dokumentaci.

##Distribuce
Projekt je šířen výhradně přes Git repozitář na serveru [BitBucker.org](https://bitbucket.org/pstefan/brs).

##Překlad
Překlad aplikace probíhá pomocí přiloženého souboru *Makefile* pomocí unixové utilitky *make*. Projekt podporuje několik targetů:

- **all** - přeloží klientskou i serverovou aplikaci (defaultní)
- **server** - přeloží pouze serverovou část
- **client** - přeloží pouze klientskou část
- **prepare** - do adresáře k přeloženým binárkám nakopíruje vzorové konfigurační soubory a soubory s plánky autobusů
- **start** - spustí jeden server, který naplní daty pomocí *fill_db.txt* a jeden klient. **VAROVÁNÍ:** tato část je závislá na balíčku *gnome-terminal*.
- **clean** - odstraní všechny soubory vzniklé překladem
- **size** - vypíše souhrnou velikost zdrojových souborů na disku
- **doc** - vygeneruje dokumentaci. Tento cíl vyžaduje nainstalovaný generátor dokumentace *Doxygen*.

###Doporučený postup
```
petr@arya$ git clone git@bitbucket.org:pstefan/brs.git
petr@arya$ make
petr@arya$ make prepare
petr@arya$ make start
```
Volitelné kroky jsou:
```
petr@arya$ make doc
petr@arya$ make size
```

##Licence
Tento program je šířen pod licencí **GNU GPLv3**, jejíž text je dostupný například [zde](http://www.gnu.org/copyleft/gpl.html). Použitá knihovna C5 je šířena pod MIT licencí ([znění zde](http://www.itu.dk/research/c5/LICENSE.txt)).
