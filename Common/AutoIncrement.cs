using System;
using System.Threading;

/// <summary>
/// Třída zajišťující vydání dalšího čísla v pořadí. Všechny metody jsou připravené na volání z více vláken.
/// </summary>
class AutoIncrement {
    /// <summary>
    /// Konstruktor. Inicializuje počítadlo jeho počáteční hodnotou.
    /// </summary>
    /// <param name="Start">Počáteční hodnota počítadla.</param>
    public AutoIncrement(uint Start = 0) {
        Lock_ = new Mutex();
        Current_ = Start;
    }
    /// <summary>
    /// Vrátí další hodnotu počítadla v pořadí.
    /// </summary>
    /// <returns>Další hodnota v pořadí.</returns>
    public uint Next() {
        uint result;
        Lock_.WaitOne();
        result = ++Current_;
        Lock_.ReleaseMutex();
        return result;
    }
    /// <summary>
    /// Nastaví počítadlo na příslušnou hodnota.
    /// </summary>
    /// <param name="Value">Požadovaná hodnota počítadla.</param>
    public void Set(uint Value) {
        Lock_.WaitOne();
        if(Value > Current_) {
            Current_ = Value;
        }
        Lock_.ReleaseMutex();
    }
    private uint Current_;
    private Mutex Lock_;
};