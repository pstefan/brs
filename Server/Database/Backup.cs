using System;

/// <summary>
/// Binární data.
/// </summary>
/// Třída obsahuje binární hlavičky pro jednotlivé typy binárních dat, které se zálohují.
class BinaryValues {
    /// <summary>
    /// Společná hlavička pro všechny položky.
    /// </summary>
    public static uint Header = 0x2b2b2b23;
    /// <summary>
    /// Pole hlaviček pro každý z typů BkHeader.DatObject
    /// </summary>
    /// @warning Pořadí v tomto poli a BkHeader.DatObject spolu musí korespondovat.
    public static uint[] HeaderValue = {
        0x20206244, // Database
        0x735f6943, // Cities
        0x735f6c41, // Aliases
        0x735f6c50, // Plans
        0x735f7542, // Buses
        0x79746943, // City
        0x61696c41, // Alias
        0x6e616c50, // Plan
        0x20737542, // Bus
        0x74616553, // Seat
        0x20646e45, // End
    };
}

/// <summary>
/// Hlavička úseku binárních dat.
/// </summary>
class BkHeader {
    /// <summary>
    /// Druhy bloků v binárních datech zálohy.
    /// </summary>
    public enum DatObject {Database, Cities, Aliases, Plans, Buses, City, Alias, Plan, Bus, Seat, End};
    /// <summary>
    /// Hodnota hlavičky bloku.
    /// </summary>
    /// <returns>binární hodnota hlavičky bloku.</returns>
    /// <param name="Object">hlavička bloku.</param>
    public uint HeaderValue(DatObject Object) {
        return BinaryValues.HeaderValue[(int)Object];
    }
};

/// <summary>
/// Pomocná třída pro zálohu databáze.
/// </summary>
/// Pomocí této třídy se provádí záloha jednotlivých objektů databáze do binárního proudu. Obsahuje
/// metody pro jednotlivé primitivní typy, složitější se pak poskládají z těchto stavebních kamenů.
/// Viz @ref backup.
class Backup : BkHeader {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="stream">Binární proud, do kterého se bude provádět záloha.</param>
    public Backup(System.IO.Stream stream) {
        stream_ = new System.IO.BinaryWriter(stream);
    }
    /// <summary>
    /// Hlavička bloku.
    /// </summary>
    /// Vytvoření hlavičky pro odpovídající druh objektu a její zapsání do výstupního proudu.
    /// <param name="Object">druh objektu určený výčtovým typem.</param>
    public void Header(DatObject Object) {
        stream_.Write(BinaryValues.Header);
        stream_.Write(HeaderValue(Object));
        stream_.Flush();
    }
    /// <summary>
    /// Záloha logické hodnoty.
    /// </summary>
    /// Hodnota typu @c bool je zapsána do výstupního proudu.
    /// <param name="Value">logická proměnná.</param>
    public void Bool(bool Value) {
        stream_.Write(Value);
        stream_.Flush();
    }
    /// <summary>
    /// Záloha neznaménkové hodnoty.
    /// </summary>
    /// Hodnota typu @c uint je zapsána do výstupního proudu.
    /// <param name="Value">neznaménková celočíselná proměnná.</param>
    public void Unsigned(uint Value) {
        stream_.Write(Value);
        stream_.Flush();
    }
    /// <summary>
    /// Záloha řetězce.
    /// </summary>
    /// Do výstupního proudu je uložen znakový řetezec. Nejprve jeho délka a poté pole znaků v kódování UTF-8.
    /// <param name="Value">řetězec znaků.</param>
    public void String(string Value) {
        byte[] str = System.Text.Encoding.UTF8.GetBytes(Value);
        uint length = (uint)str.Length;
        stream_.Write(length);
        stream_.Write(str);
        stream_.Flush();
    }
    /// <summary>
    /// Záloha času.
    /// </summary>
    /// Do výstupního proudu uloží čas v binární podobě jako @c Int64.
    /// <param name="time">čas.</param>
    public void Time(DateTime time) {
        stream_.Write(time.ToBinary());
        stream_.Flush();
    }
    private System.IO.BinaryWriter stream_;
};

/// <summary>
/// Pomocná třída pro obnovení databáze ze zálohy.
/// </summary>
/// Pomocí této třídy se provádí obnova jednotlivých objektů databáze z binárního proudu. Obsahuje
/// metody pro jednotlivé primitivní typy, složitější se pak poskládají z těchto stavebních kamenů.
/// Viz @ref backup.
class Restore : BkHeader {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="stream">vstupní proud se zálohou.</param>
    public Restore(System.IO.Stream stream) {
        stream_ = new System.IO.BinaryReader(stream);
    }
    /// <summary>
    /// Hlavička objektu.
    /// </summary>
    /// Načtení a kontrola hlavičky pro požadovaný objekt.
    /// <param name="Object">druh objektu určený výčtovým typem.</param>
    public void Header(DatObject Object) {
        uint header = stream_.ReadUInt32();
        uint result = stream_.ReadUInt32();
        if(header != BinaryValues.Header || result != HeaderValue(Object)) {
            throw new ArgumentException();
        }
    }
    /// <summary>
    /// Načtení logické hodnoty.
    /// </summary>
    /// Z proudu je načtena logická hodnota.
    /// <returns>načtená hodnota @e bool</returns>
    public bool Bool() {
        return stream_.ReadBoolean();
    }
    /// <summary>
    /// Načtení neznaménkové hodnoty.
    /// </summary>
    /// Z proudu je načtena celočíselná neznaménková hodnota.
    /// <returns>načtená hodnota @e uint</returns>
    public uint Unsigned() {
        return stream_.ReadUInt32();
    }
    /// <summary>
    /// Načtení řetězce.
    /// </summary>
    /// Z proudu je načten řetězec znaků. Formát načítaných dat je stejný jako u Backup.String.
    /// <returns>načtený řetězec znaků</returns>
    public string String() {
        uint length = stream_.ReadUInt32();
        byte[] str = stream_.ReadBytes((int)length);
        return System.Text.Encoding.UTF8.GetString(str);
    }
    /// <summary>
    /// Načtení času.
    /// </summary>
    /// Z proudu je načten Int64, který je naparsován do typu DateTime.
    /// <returns>načtená hodnota @e DateTime</returns>
    public DateTime Time() {
        long data = stream_.ReadInt64();
        return DateTime.FromBinary(data);
    }
    private System.IO.BinaryReader stream_;
};