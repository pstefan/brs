using System;
using System.Collections.Generic;

/// <summary>
/// Výčtový typ jehož položky odpovídají jednotlivým příkazům databáze.
/// </summary>
enum QueryCmd {
    CMDADDCITY, CMDADDCITYALIAS, CMDDELETECITY, CMDFINDCITY,
    CMDCITYINFO, CMDADDBUSPLAN, CMDDELETEBUSPLAN, CMDFINDBUSPLAN,
    CMDBUSPLANINFO, CMDADDBUS, CMDDELETEBUS, CMDFINDBUS,
    CMDBUSINFO, CMDENABLEBUS, CMDDISABLEBUS, CMDNEWCUSTOMER,
    CMDASKSEATS, CMDLOCKSEATS, CMDUNLOCKSEATS, CMDNEXTBUS,
    CMDNEXTCITY, CMDNEXTPLAN, CMDLISTCITY, CMDLISTALIAS
}


//! Informace o městě
class CityInfo {
    //! ID města
    public uint Id;
    //! název města
    public string Name;
};

//! Informace o plánu autobusu
class PlanInfo {
    //! ID plánu
    public uint Id;
    //! název plánu nebo typu autobusu
    public string Name;
    //! počet sedadel
    public uint Seats;
};

//! Informace o autobusu
class BusInfo {
    //! ID autobusu
    public uint Id;
    //! číslo linky autobusu
    public uint Line;
    //! číslo počátečního města
    public uint CityFrom;
    //! číslo cílového města
    public uint CityTo;
    //! číslo plánu
    public uint Plan;
    //! čas odjezdu z výchozí stanice
    public DateTime LeaveTime;
};


/// <summary>
/// Klientské rozhraní mezi databází a zbytkem programu.
/// </summary>
interface IQuery {
    /// <summary>
    /// přidání města.
    /// </summary>
    /// <returns>unikátní číslo nově přidaného města či číslo jiz dříve existujícího města.</returns>
    /// <param name="Name">jméno města.</param>
    uint AddCity(string Name);
    /// <summary>
    /// přidání zkratky k nějakému existujícímu městu.
    /// </summary>
    /// <returns>úspěch operace - @e false v případě, že jméno již existuje (jako město či alias) nebo pokud město s ID není v databázi.</returns>
    /// <param name="Name">název zkratky či dalšího názvu.</param>
    /// <param name="Id">ID existujícího města, se kterým chceme propojit tuto zkratku.</param>
    bool AddCityAlias(string Name, uint Id);
    /// <summary>
    /// smazání města podle jména.
    /// </summary>
    /// Provede smazání města z kontajneru a zároveň vymazání všech případných aliasů názvu města.
    /// <returns>úspěch operace - @e false v případě, že město není v databázi nebo existují autobusové linky, které město používají.</returns>
    /// <param name="Name">název či alias města.</param>
    bool DeleteCity(string Name);
    /// <summary>
    /// nalezení města podle názvu.
    /// </summary>
    /// Hledání města probíhá nejprve v normálních názvech měst a v případě neúspěchu ve zkratkách (aliasech)
    /// <returns>ID nalezeného města nebo 0 v případě neúspěchu.</returns>
    /// <param name="Name">název či alias města.</param>
    uint FindCity(string Name);
    /// <summary>
    /// získání informací o městě.
    /// </summary>
    /// <returns>úspěch operace - @e false, pokud město nebylo nalezeno.</returns>
    /// <param name="City">ID města.</param>
    /// <param name="Info">struktura @ref CityInfo vyplněná daty.</param>
    /// <param name="LinesOut">počet autobusů odjíždějících z tohoto města.</param>
    /// <param name="LinesIn">počet autobusů přijíždějících do tohoto města.</param>
    bool CityInfo(uint City, out CityInfo Info, out uint LinesOut, out uint LinesIn);
    /// <summary>
    /// přidání plánu autobusu.
    /// </summary>
    /// <returns>unikátní číslo přidaného plánu nebo číslo již existujícího plány se zadaným jménem.</returns>
    /// <param name="Name">název plánu/typu autobusu.</param>
    /// <param name="Seats">počet sedadel.</param>
    /// <param name="Data">rozvržení sedadel.</param>
    uint AddBusPlan(string Name, uint Seats, string Data);
    /// <summary>
    /// smazání plánu autobusu.
    /// </summary>
    /// @note Vymazání plánu autobusu z kontajneru, může být provedeno pouze v případě, že neexistuje žádný autobus s tímto plánem.
    /// 
    /// <returns>úspěch operace.</returns>
    /// <param name="Name">název plánu autobusu.</param>
    bool DeleteBusPlan(string Name);
    /// <summary>
    /// nalezení plánu autobusu.
    /// </summary>
    /// Hledání plánu autobusu podle zadaného jména.
    /// <returns>ID plánu, 0 v případě neúspěchu.</returns>
    /// <param name="Name">název plánu autobusu.</param>
    uint FindBusPlan(string Name);
    /// <summary>
    /// získání informací o plánu.
    /// </summary>
    /// <returns>úspěch operace - @e false, pokud plán nebyl nalezen.</returns>
    /// <param name="Plan">ID plánu autobusu.</param>
    /// <param name="Info">struktura @ref PlanInfo k naplnění daty.</param>
    /// <param name="Buses">počet autobusů využívajících tento plán.</param>
    /// <param name="Data">data s rozložením sedadel.</param>
    bool BusPlanInfo(uint Plan, out PlanInfo Info, out uint Buses, out string Data);
    /// <summary>
    /// přidání autobusu.
    /// </summary>
    /// Vytvoření autobusu s uvedenými daty a jeho přidání do databáze. Identifikační číslo (položka @c Id struktury @c BusInfo) nemusí být vyplněno,
    /// neboť dojde k jeho naplnění jedinečným číslem až při zpracování požadavku.
    /// <returns>ID nově vloženého autobusu nebo 0 v případě chyby (snaha o zavedení duplicitního autobusu).</returns>
    /// <param name="Bus">struktura @ref BusInfo s daty potřebnými k vytvoření nového autobusu.</param>
    uint AddBus(BusInfo Bus);
    /// <summary>
    /// smazání autobusu.
    /// </summary>
    /// <returns>úspěch operace.</returns>
    /// <param name="Bus">ID autobusu určeného ke smazání.</param>
    bool DeleteBus(uint Bus);
    /// <summary>
    /// nalezení autobusu.
    /// </summary>
    /// Funkce pro nalezení autobusu z města @a From do města @a To a s časem odjezdu rovným nebo nejbližším větším času @a LeaveTime.
    /// <returns>ID nalezeného autobusu nebo 0.</returns>
    /// <param name="From">ID výchozího města.</param>
    /// <param name="To">ID cílového města.</param>
    /// <param name="LeaveTime">čas, od kterého hledáme shodu (tj. dolní hranice času odjezdu).</param>
    uint FindBus(uint From, uint To, DateTime LeaveTime);
    /// <summary>
    /// získání informací o autobusu.
    /// </summary>
    /// <returns>úspěch operace - @e false, nebyl-li autobus nalezen.</returns>
    /// <param name="uiBus">ID autobusu.</param>
    /// <param name="Info">struktura @ref BusInfo, která bude naplněná daty.</param>
    /// <param name="Enabled">přepínač, zda jsou povoleny změny v autobusu.</param>
    /// <param name="Seats">počet sedadel autobusu.</param>
    /// <param name="FreeSeats">seznam volných sedadel.</param>
    bool BusInfo(uint uiBus, out BusInfo Info, out bool Enabled, out uint Seats, out Queue<uint> FreeSeats);
    /// <summary>
    /// povolení autobusu.
    /// </summary>
    /// Povolí provádět změny v rezervacích.
    /// <returns>úspěch operace - @e false, nebyl-li autobus nalezen.</returns>
    /// <param name="Bus">ID autobusu.</param>
    bool EnableBus(uint Bus);
    /// <summary>
    /// zakázání autobusu.
    /// </summary>
    /// Zakáže provádět změny v rezervacích.
    /// <returns>úspěch operace - @e false, nebyl-li autobus nalezen.</returns>
    /// <param name="Bus">ID autobusu.</param>
    bool DisableBus(uint Bus);
    /// <summary>
    /// unikátní ID pro nového zákazníka.
    /// </summary>
    /// Operace rezervace sedadel je vždy vázána na konkrétního zákazníka identifikovaného jedinečným číslem.
    /// Díky tomu pak může zákazník své rezervace dodatečně zrušit nebo potvrdit dočasnou rezervaci.
    /// <returns>unikátní číslo zákazníka.</returns>
    uint NewCustomer();
    /// <summary>
    /// dočasné zamluvení sedadel.
    /// </summary>
    /// Dočasné zamluvení sedadel, které po určitém čase vyprší.
    /// <returns>úspěch operace - @e false, pokud autobus není nalezen nebo se operace nezdařila pro všechna sedadla.</returns>
    /// <param name="Bus">ID autobusu.</param>
    /// <param name="Seats">seznam sedadel k rezervaci, neúspěšná sedadla budou smazána ze seznamu.</param>
    /// <param name="Customer">ID zákazníka.</param>
    bool AskSeats(uint Bus, ref Queue<uint> Seats, uint Customer);
    /// <summary>
    /// trvalá rezervace sedadel.
    /// </summary>
    /// <returns>úspěch operace - @e false, pokud autobus není nalezen nebo se operace nezdařila pro všechna sedadla.</returns>
    /// <param name="Bus">ID autobusu.</param>
    /// <param name="Seats">seznam sedadel k rezervaci, neúspěšná sedadla budou smazána ze seznamu.</param>
    /// <param name="Customer">ID zákazníka.</param>
    bool LockSeats(uint Bus, ref Queue<uint> Seats, uint Customer);
    /// <summary>
    /// zrušení rezervace sedadel.
    /// </summary>
    /// @warning K úspěšnému zrušení rezervace je nutné mít zákaznické ID, se kterým byla provedena rezervace.
    ///
    /// <returns>úspěch operace - @e false, pokud autobus není nalezen nebo se operace nezdařila pro všechna sedadla.</returns>
    /// <param name="Bus">ID autobusu.</param>
    /// <param name="Seats">seznam sedadel k uvolnění, neúspěšná sedadla budou smazána ze seznamu.</param>
    /// <param name="Customer">ID zákazníka.</param>
    bool UnlockSeats(uint Bus, ref Queue<uint> Seats, uint Customer);
    /// <summary>
    /// nalezení dalšího autobusu v pořadí.
    /// </summary>
    /// Najde další autobus podle ID.
    /// 
    /// Pomocí této metody je možné sekvenčně procházet kontajner autobusů (zapouzdřený ve třídě 
    /// @ref Buses). Odpovídající kousek kódu může vypadat např. následovně
    /// 
    /// <code>
    /// uint Id; @n
    /// while((Id = query.NextBus(Id)) != 0) { @n
    ///    // zpracování autobusu s číslem Id @n
    /// }
    /// </code>
    /// 
    /// Metodu můžeme využít např. v situaci, kdy potřebujeme zakázat rezervace ve všech autobusech,
    /// které odjíždějí během následující hodiny (abychom mohli předat řidičům konečný seznam 
    /// rezervovaných sedadel).
    /// <returns>ID nalezeného autobusu nebo @e 0.</returns>
    /// <param name="Bus">ID autobusu, od kterého hledáme následující.</param>
    uint NextBus(uint Bus);
    /// <summary>
    /// nalezení dalšího města v pořadí.
    /// </summary>
    /// Najde další město podle ID.
    /// <returns>ID nalezeného města nebo @e 0.</returns>
    /// <param name="City">ID města, od kterého hledáme následující.</param>
    uint NextCity(uint City);
    /// <summary>
    /// nalezení dalšího plánu v pořadí.
    /// </summary>
    /// Najde další plán podle ID.
    /// <returns>ID nalezeného plánu nebo @e 0.</returns>
    /// <param name="Plan">ID plánu, od kterého hledáme následující.</param>
    uint NextPlan(uint Plan);
    /// <summary>
    /// nalezení dalšího města v abecedním pořadí.
    /// </summary>
    /// Najde další město v abecedním pořadí.
    /// <returns>úspěch operace (@e false pro dosažení konce).</returns>
    /// <param name="From">název města, od kterého hledáme následující.</param>
    /// <param name="Name">jméno dalšího města v pořadí.</param>
    bool ListCity(string From, out string Name);
    /// <summary>
    /// nalezení dalšího aliasu města v abecedním pořadí.
    /// </summary>
    /// Najde další alias v abecedním pořadí.
    /// <returns>úspěch operace (@e false pro dosažení konce).</returns>
    /// <param name="From">alias, od kterého hledáme následující.</param>
    /// <param name="Alias">alias dalšího města v pořadí.</param>
    /// <param name="City">jméno města odpovídající aliasu.</param>
    bool ListAlias(string From, out string Alias, out string City);
}
