﻿using System;
using System.Net.Sockets;


/// <summary>
/// klientská část pro komunikaci pomocí TCP protokolu.
/// </summary>
class ClientConnection : NetworkConnection {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="Server">Doménové jméno nebo IP adresa serveru.</param>
    /// <param name="Port">Port serveru.</param>
    public ClientConnection(string Server, int Port) {
        Server_ = Server;
        Port_ = Port;
        Client_ = null;
    }
    /// <summary>
    /// Navázání spojení se serverem.
    /// </summary>
    /// <returns>@e true v případě úspěchu (jinak vznikne výjimka při volání <see cref="TcpClient.Connect"/>).</returns>
    public bool ConnectServer() {
        Client_ = new TcpClient();
        //Client_.ReceiveTimeout = 10000; // 10 s
        //Client_.SendTimeout = 10000; // 10 s
        Client_.NoDelay = true;
        System.Net.IPAddress address;
        if(System.Net.IPAddress.TryParse(Server_, out address)) {
            Client_.Connect(address, Port_);
        }
        else {
            Client_.Connect(Server_, Port_);
        }
        Reader_ = new System.IO.BinaryReader(Client_.GetStream());
        Writer_ = new System.IO.BinaryWriter(Client_.GetStream());
        return true;
    }
    /// <summary>
    /// Zavření spojení se serverem.
    /// </summary>
    public void DisconnectServer() {
        Client_.Close();
        Client_ = null;
    }
    private TcpClient Client_;
    private string Server_;
    private int Port_;
};