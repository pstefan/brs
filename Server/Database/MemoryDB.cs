using System;
using System.Collections.Generic;
using System.Threading;
using C5;

/// <summary>
/// Porovnávací objekt pro autobusy.
/// </summary>
/// Řadí podle města odjezdu, města příjezdu a času odjezdu. Využívá se při hledání autobusů uživatelem.
class BusLess : IComparer<Bus> {
    /// <summary>
    /// Porovnání dvou autobusů.
    /// </summary>
    /// <param name="x">První autobus.</param>
    /// <param name="y">Druhý autobus.</param>
    public int Compare(Bus x, Bus y) {
        if(x.CityFrom() < y.CityFrom())
            return -1;
        else if(x.CityFrom() == y.CityFrom() && x.CityTo() < y.CityTo())
            return -1;
        else if(x.CityFrom() == y.CityFrom() && x.CityTo() == y.CityTo() && x.LeaveTime() < y.LeaveTime())
            return -1;
        else if(x.CityFrom() == y.CityFrom() && x.CityTo() == y.CityTo() && x.LeaveTime() == y.LeaveTime())
            return 0;
        else
            return 1;
    }
}

/// <summary>
/// Hlavní třída databáze.
/// </summary>
/// Třída obsahuje metody pro práci s autobusy, městy a plány autobusů. Implementuje rozhraní @ref IQuery
/// pro klientská volání a @ref IAdmin pro správu
/// a zapouzdřuje tak veškeré operace s databází. Třída vlastně obsahuje jak vlastní databázový stroj, tak
/// zajišťuje uložení dat pomocí kontajnerů C5 (tedy přímo v operační paměti počítače) - viz @ref server. Kromě vlastních 
/// datových kontajnerů pro města, plány a autobusy implementuje i pomocné kontajnery (indexy) pro rychlé 
/// vyhledávání podle různých kritérií. Pro snazší vyhledávání měst podporuje i jejich pomocná jména 
/// (aliasy).
/// @image html Db.png "Databáze z pohledu jejího uživatele a správce"
/// Základní entity, se kterými databáze pracuje, jsou reprezentovány třídami:
/// @li @ref City - město jako počáteční a koncová stanice autobusové linky,
/// @li @ref Plan - typ autobusu, tj. počet sedadel pro cestující a jejich rozmístění,
/// @li @ref Seat - sedadlo v autobuse a stav jeho rezervace,
/// @li @ref Bus - autobus, který sdružuje kolekci sedadel.
/// Počet sedadel v konkrétním autobusu je dán jeho plánem a v průběhu životnosti je pevně daný, přístup
/// k jednotlivým sedadlům je pomocí indexu, pro uložení je zvolen <tt>List<Seat></tt>. Ostatní třídy
/// jsou uloženy ve stromové struktuře, která zaručuje rychlé vyhledávání. Použitým kontajnerem je
/// <tt>TreeDictionary</tt> (např. tedy <tt>TreeDictionary<string, City></tt>), setřídění je podle unikátního čísla. Pro 
/// vyhledávání podle dodatečných kritérií (např. města dle názvu nebo autobusu dle koncových měst a času 
/// odjezdu) jsou součástí databáze příslušné indexy, také kontajnery typu <tt>TreeDictionary</tt>, které jsou však 
/// setříděny podle požadovaného kritéria. 
/// Aliasy měst jsou implementovány jako <tt>TreeDictionary<string, uint></tt>.
/// Objekty @ref City, @ref Plan a @ref Bus obecně nejsou po dobu svého života konstantní. Přesto mohou být
/// bez problémů uloženy v asociativních kontajnerech, neboť položky použité pro uspořádání konstantní 
/// zůstávají (tyto jsou inicializovány v konstruktoru a následně již není možné je měnit).
/// Předpokládá se, že k databázi může současně přistupovat více klientů najednou (tj. volání metod může
/// probíhat současně z více vláken). Proto je implementace třídy vybavena zámky typu <i>více čtení/jeden
/// zápis</i>, které na jedné straně zajišťují konzistenci dat, na druhé se snaží minimalizovat prostoje
/// (podrobněji viz @ref db_lock). 
/// Řešením otázky bezpečnosti uložení dat v operační paměti se podrobněji zabývá kapitola @ref journal.
class MemoryDb : IQuery, IAdmin {
    /// <summary>
    /// Výchozí konstruktor.
    /// </summary>
    /// Vytvoření prázdné databáze, tj. inicializace všech součástí a vytvoření prázdných kontajnerů
    /// měst, jejich aliasů, plánů a autobusů včetně prázdných indexů.
    public MemoryDb() {
       Init();
       Cities_ = new Cities(); // města
       CityAlias_ = new TreeDictionary<string, uint>(); // náhradní jména pro města
       Plans_ = new Plans(); // plány autobusů
       Buses_ = new Buses(); // autobusy
    }
    /// <summary>
    /// Konstruktor obnovení ze zálohy.
    /// </summary>
    /// Databáze je vytvořena ve stavu, který odpovídá dříve provedené záloze (pomocí metody @ref MemoryDb.Backup).
    /// Všechny indexy jsou správně synchronizovány s vlastními datovými kontajnery.
    ///
    /// Pokud při obnově ze zálohy dojde k nějaké chybě (nesprávný formát dat, chyba při čtení z proudu apod.)
    /// je vytvořena prázdná databáze.
    ///
    /// Více viz @ref backup.
    /// <param name="stream">proud se zálohou.</param>
    public MemoryDb(System.IO.Stream stream) {
        Init();
        // kontajnery budou načteny z proudu
        Cities_ = null; // města
        CityAlias_ = null; // náhradní jména pro města
        Plans_ = null; // plány autobusů
        Buses_ = null; // autobusy
        // načtení z proudu v chráněném bloku - vyvolání výjimky při selhání kterékoliv podoperace
        try {
            Restore restore = new Restore(stream);
            // hlavička
            restore.Header(BkHeader.DatObject.Database);
            Increment_.Set(restore.Unsigned()); // počítadlo zákazníků
            // města
            Cities_ = new Cities(stream);
            // aliasy měst
            CityAlias_ = new TreeDictionary<string, uint>();
            restore.Header(BkHeader.DatObject.Aliases);
            uint Count = restore.Unsigned();
            for(uint i = 0; i < Count; i++) {
                restore.Header(BkHeader.DatObject.Alias);
                string TempAlias = restore.String();
                uint TempId = restore.Unsigned();
                CityAlias_.Add(TempAlias, TempId);
            }
            // plány
            Plans_ = new Plans(stream);
            // autobusy
            Buses_ = new Buses(stream);
            // konec
            restore.Header(BkHeader.DatObject.End);
        }
        catch(Exception) {
            // vytvoření neexistujících nebo vyprázdnění již vytvořených kontajnerů v případě chyby
            if(Cities_ == null) // města
                Cities_ = new Cities();
            else
                Cities_.Clear();
            if(CityAlias_ == null) // náhradní jména pro města
                CityAlias_ = new TreeDictionary<string, uint>();
            else
                CityAlias_.Clear();
            if(Plans_ == null) // plány autobusů
                Plans_ = new Plans();
            else
                Plans_.Clear();
            if(Buses_ == null) // autobusy
                Buses_ = new Buses();
            else
                Buses_.Clear();
        }
        uint Id = 0;
        // vytvoření indexů měst
        while((Id = NextCity(Id)) != 0) {
            var city = Cities_.Find(Id);
            CityIndex_.Add(city.Name(), city);
        }
        // vytvoření indexů plánů
        while((Id = NextPlan(Id)) != 0) {
            var plan = Plans_.Find(Id);
            PlanIndex_.Add(plan.Name(), plan);
        }
        // vytvoření indexů autobusů
        while((Id = NextBus(Id)) != 0) {
            var bus = Buses_.Find(Id);
            BusIndex_.Add(bus);
        }
    }
    // rozhraní Query
    public uint AddCity(string Name) {
        uint CityId;
        City City;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockCities_.EnterWriteLock(); // subsystém měst
        CityId = FindCityNoLock(Name); // nalezení města dle jména
        if(CityId == 0) {
            // pro nové město vytvoření instance třídy v kontajneru
            City = Cities_.Add(Name);
            // přidání odkazu do indexu
            CityIndex_.Add(Name, City);
            CityId = City.Id();
            if(Journal_ != null) { // zápis změny databáze do žurnálu
                Journal_.Write(QueryCmd.CMDADDCITY, Name);
            }
        }
        LockCities_.ExitWriteLock(); // subsystém měst
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return CityId;
    }
    public bool AddCityAlias(string Name, uint Id) {
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockCities_.EnterWriteLock(); // subsystém měst
        if(FindCityNoLock(Name) != 0 || Cities_.Find(Id) == null)
            result = false; // město pod jménem či aliasem již existuje nebo neexistuje podle čísla
        else { // vložení nového aliasu
            CityAlias_.Add(Name, Id);
            result = true;
            if(result && Journal_ != null) { // zápis změny databáze do žurnálu
                Journal_.Write(QueryCmd.CMDADDCITYALIAS, Name, Id);
            }
        }
        LockCities_.ExitWriteLock(); // subsystém měst
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool DeleteCity(string Name) {
        uint City;
        CityInfo Info;
        uint LinesOut;
        uint LinesIn;
        bool result;

        result = false;
        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockCities_.EnterWriteLock(); // subsystém měst
        City = FindCityNoLock(Name); // nalezení města dle jména
        if(City != 0) {
            // kontrola, zda město není využito některým autobusem
            CityInfoNoLock(City, out Info, out LinesOut, out LinesIn);
            if(LinesOut == 0 && LinesIn == 0) {
                CityIndex_.Remove(Name); // vymazání z indexu
                Cities_.Delete(City); // vymazání instance třídy
                // vymazání aliasů (lineární složitost)
                // musime provadet pres pomocnou kolekci, aliasy si nelze mazat v cyklu primo pod rukama
                // - jinak vyjimka C5.CollectionModifiedException
                List<string> to_delete = new List<string>();
                foreach(var alias in CityAlias_) {
                    if(alias.Value == City)
                        to_delete.Add(alias.Key);
                }
                foreach(var item in to_delete) {
                    CityAlias_.Remove(item);
                }
                to_delete = null;

                result = true;
                if(Journal_ != null) { // zápis změny databáze do žurnálu
                    Journal_.Write(QueryCmd.CMDDELETECITY, Name);
                }
            }
        }
        LockCities_.ExitWriteLock(); // subsystém měst
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public uint FindCity(string Name) {
        uint result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockCities_.EnterReadLock(); // subsystém měst
        result = FindCityNoLock(Name);
        LockCities_.ExitReadLock(); // subsystém měst
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool CityInfo(uint City, out CityInfo Info, out uint LinesOut, out uint LinesIn) {
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockCities_.EnterReadLock(); // subsystém měst
        result = CityInfoNoLock(City, out Info, out LinesOut, out LinesIn);
        LockCities_.ExitReadLock(); // subsystém měst
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public uint AddBusPlan(string Name, uint Seats, string Data) {
        uint PlanId;
        Plan Plan;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockPlans_.EnterWriteLock(); // subsystém plánů
        PlanId = FindBusPlanNoLock(Name); // nalezení plánu dle jména
        if(PlanId == 0) {
            // pro nový plán vytvoření instance třídy v kontajneru
            Plan = Plans_.Add(Name, Seats, Data);
            // přidání odkazu do indexu
            PlanIndex_.Add(Plan.Name(), Plan);
            PlanId = Plan.Id();
            if(Journal_ != null) { // zápis změny databáze do žurnálu
                Journal_.Write(QueryCmd.CMDADDBUSPLAN, Name, Seats, Data);
            }
        }
        LockPlans_.ExitWriteLock(); // subsystém plánů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return PlanId;
    }
    public bool DeleteBusPlan(string Name) {
        uint Plan;
        PlanInfo Info;
        uint Buses;
        bool result;
        string str;

        result = false;
        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockPlans_.EnterWriteLock(); // subsystém plánů
        Plan = FindBusPlanNoLock(Name); // nalezení plánu dle jména
        if(Plan != 0) {
            // kontrola, zda plán není využit některým autobusem
            BusPlanInfoNoLock(Plan, out Info, out Buses, out str);
            if(Buses == 0) {
                PlanIndex_.Remove(Name); // vymazání z indexu
                Plans_.Delete(Plan); // vymazání instance třídy
                result = true;
                if(Journal_ != null) { // zápis změny databáze do žurnálu
                    Journal_.Write(QueryCmd.CMDDELETEBUSPLAN, Name);
                }
            }
        }
        LockPlans_.ExitWriteLock(); // subsystém plánů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public uint FindBusPlan(string Name) {
        uint result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockPlans_.EnterReadLock(); // subsystém plánů
        result = FindBusPlanNoLock(Name);
        LockPlans_.ExitReadLock(); // subsystém plánů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool BusPlanInfo(uint Plan, out PlanInfo Info, out uint Buses, out string Data) {
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockPlans_.EnterReadLock(); // subsystém plánů
        result = BusPlanInfoNoLock(Plan, out Info, out Buses, out Data);
        LockPlans_.ExitReadLock(); // subsystém plánů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public uint AddBus(BusInfo Bus) {
        Bus bus;
        uint BusId;
        City CityFrom;
        City CityTo;
        Plan Plan;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterWriteLock(); // subsystém autobusů
        BusId = FindBusNoLock(Bus.CityFrom, Bus.CityTo, Bus.LeaveTime); // hledání v indexu
        if(BusId == 0 || Buses_.Find(BusId).LeaveTime() != Bus.LeaveTime) { // takový autobus dosud neexistuje
            // kontrola, zda výchozí i cílové město a plán existují
            LockCities_.EnterReadLock(); // subsystém měst
            LockPlans_.EnterReadLock(); // subsystém plánů
            CityFrom = Cities_.Find(Bus.CityFrom);
            CityTo = Cities_.Find(Bus.CityTo);
            Plan = Plans_.Find(Bus.Plan);
            if(CityFrom != null && CityTo != null && Plan != null) {
                bus = Buses_.Add(Bus, Plan.Seats()); // vytvoření nového autobusu
                BusIndex_.Add(bus); // aktualizace indexu
                // aktualizace dalších dat
                CityFrom.LinesOut(+1);
                CityTo.LinesIn(+1);
                Plan.Buses(+1);
                BusId = bus.Id();
                if(Journal_ != null) { // zápis změny databáze do žurnálu
                    Journal_.Write(QueryCmd.CMDADDBUS, Bus);
                }
            }
            LockPlans_.ExitReadLock(); // subsystém plánů
            LockCities_.ExitReadLock(); // subsystém měst
        }
        LockBuses_.ExitWriteLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return BusId;  
    }
    public bool DeleteBus(uint Bus) {
        Bus bus;
        City city;
        Plan plan;
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterWriteLock(); // subsystém autobusů
        bus = Buses_.Find(Bus); // nalezení autobusu dle čísla
        if(bus == null)
            result = false;
        else {
            // aktualizace dat v dalších kontajnerech (položky by vždy měly existovat)
            LockCities_.EnterReadLock(); // subsystém měst
            city = Cities_.Find(bus.CityFrom());
            if(city != null)
                city.LinesOut(-1);
            city = Cities_.Find(bus.CityTo());
            if(city != null)
                city.LinesIn(-1);
            LockCities_.ExitReadLock(); // subsystém měst
            LockPlans_.EnterReadLock(); // subsystém plánů
            plan = Plans_.Find(bus.Plan());
            if(plan != null)
                plan.Buses(-1);
            LockPlans_.ExitReadLock(); // subsystém plánů
            // zrušení instance třídy i aktualizace indexu
            BusIndex_.Remove(bus);
            Buses_.Delete(Bus);
            result = true;
            if(Journal_ != null) {// zápis změny databáze do žurnálu
                Journal_.Write(QueryCmd.CMDDELETEBUS, Bus);
            }
        }
        LockBuses_.ExitWriteLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public uint FindBus(uint From, uint To, DateTime LeaveTime) {
        uint result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterReadLock(); // subsystém autobusů
        result = FindBusNoLock(From, To, LeaveTime);
        LockBuses_.ExitReadLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool BusInfo(uint Bus, out BusInfo Info, out bool Enabled, out uint Seats, out Queue<uint> FreeSeats) {
        Bus bus;
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterReadLock(); // subsystém autobusů
        bus = Buses_.Find(Bus); // nalezení podle čísla
        if(bus != null) {
            //if(Info != null)
            bus.GetInfo(out Info); // konstatní informace
            //if(Enabled != null)
            Enabled = bus.Enabled(); // možnost provádění rezervací
            //if(Seats != null)
            Seats = bus.Seats(); // počet sedadel
            //if(FreeSeats != null)
            bus.FreeSeats(out FreeSeats); // aktuálně volná sedadla
            result = true;
        }
        else {
            result = false;
            Info = null;
            Enabled = false;
            Seats = 0;
            FreeSeats = null;
        }
        LockBuses_.ExitReadLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool EnableBus(uint Bus) {
        Bus bus;
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterReadLock(); // subsystém autobusů
        bus = Buses_.Find(Bus);
        if(bus != null) {
            bus.Enable();
            result = true;
            if(Journal_ != null) { // zápis změny databáze do žurnálu
                Journal_.Write(QueryCmd.CMDENABLEBUS, Bus);
            }
        }
        else
            result = false;
        LockBuses_.ExitReadLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool DisableBus(uint Bus) {
        Bus bus;
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterReadLock(); // subsystém autobusů
        bus = Buses_.Find(Bus);
        if(bus != null) {
            bus.Disable();
            result = true;
            if(Journal_ != null) {// zápis změny databáze do žurnálu
                Journal_.Write(QueryCmd.CMDDISABLEBUS, Bus);
            }
        }
        else
            result = false;
        LockBuses_.ExitReadLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public uint NewCustomer() {
        uint result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        result = Increment_.Next(); // nové unikátní číslo zákazníka
        if(Journal_ != null) { // zápis změny databáze do žurnálu
            Journal_.Write(QueryCmd.CMDNEWCUSTOMER);
        }
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool AskSeats(uint Bus, ref Queue<uint> Seats, uint Customer) {
        Bus bus;
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterReadLock(); // subsystém autobusů
        bus = Buses_.Find(Bus); // nalezení autobusu dle čísla
        if(bus == null) { // autobus nenalezen
            Seats.Clear(); // vymazání všech sedadel
            result = false;
        }
        else
            result = bus.AskSeats(ref Seats, Customer); // provedení operace
        LockBuses_.ExitReadLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool LockSeats(uint Bus, ref Queue<uint> Seats, uint Customer) {
        Bus bus;
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterReadLock(); // subsystém autobusů
        bus = Buses_.Find(Bus); // nalezení autobusu dle čísla
        if(bus == null) { // autobus nenalezen
            Seats.Clear(); // vymazání všech sedadel
            result = false;
        }
        else {
            result = bus.LockSeats(ref Seats, Customer); // provedení operace
            if(Seats.Count != 0 && Journal_ != null) { // zápis změny databáze do žurnálu
                Journal_.Write(QueryCmd.CMDLOCKSEATS, Bus, Customer, Seats);
            }
        }
        LockBuses_.ExitReadLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool UnlockSeats(uint Bus, ref Queue<uint> Seats, uint Customer) {
        Bus bus;
        bool result;

        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterReadLock(); // subsystém autobusů
        bus = Buses_.Find(Bus); // nalezení autobusu dle čísla
        if(bus == null) { // autobus nenalezen
            Seats.Clear(); // vymazání všech sedadel
            result = false;
        }
        else {
            result = bus.UnlockSeats(ref Seats, Customer); // provedení operace
            if(Seats.Count != 0 && Journal_ != null) { // zápis změny databáze do žurnálu
                Journal_.Write(QueryCmd.CMDUNLOCKSEATS, Bus, Customer, Seats);
            }
        }
        LockBuses_.ExitReadLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public uint NextBus(uint Bus) {
        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockBuses_.EnterReadLock(); // subsystém autobusů
        uint result = Buses_.GetNextId(Bus);
        LockBuses_.ExitReadLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public uint NextCity(uint City) {
        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockCities_.EnterReadLock(); // subsystém měst
        uint result = Cities_.GetNextId(City);
        LockCities_.ExitReadLock(); // subsystém měst
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public uint NextPlan(uint Plan) {
        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockPlans_.EnterReadLock(); // subsystém plánů
        uint result = Plans_.GetNextId(Plan);
        LockPlans_.ExitReadLock(); // subsystém plánů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool ListCity(string From, out string Name) {
        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockCities_.EnterReadLock(); // subsystém měst
        bool result = false;
        C5.KeyValuePair<string, City> pair;
        if(CityIndex_.TryWeakSuccessor(From, out pair)) {
            Name = pair.Value.Name();
            result = true;
        }
        else {
            Name = null;
        }
        LockCities_.ExitReadLock(); // subsystém měst
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    public bool ListAlias(string From, out string Alias, out string City) {
        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockCities_.EnterReadLock(); // subsystém měst
        bool result = false;
        C5.KeyValuePair<string, uint> pair;
        if(CityAlias_.TryWeakSuccessor(From, out pair)) {
            Alias = pair.Key;
            City city = Cities_.Find(pair.Value);
            City = city.Name();
            result = true;
        }
        else {
            Alias = null;
            City = null;
        }
        LockCities_.ExitReadLock(); // subsystém měst
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return result;
    }
    // rozhraní Admin
    public bool Backup(System.IO.Stream stream) {
        LockDatabase_.EnterWriteLock(); // zámek celé databáze - výlučný přístup
        bool result = BackupNoLock(stream);
        LockDatabase_.ExitWriteLock(); // zámek celé databáze
        return result;
    }
    public bool Info(out DbInfo Info) {
        Info = new DbInfo();
        LockDatabase_.EnterReadLock(); // zámek celé databáze
        LockCities_.EnterReadLock();// subsystém měst
        Info.Cities = Cities_.Count();
        Info.CityAliases = (uint)CityAlias_.Count;
        LockCities_.ExitReadLock(); // subsystém měst
        LockPlans_.EnterReadLock(); // subsystém plánů
        Info.Plans = Plans_.Count();
        LockPlans_.ExitReadLock(); // subsystém plánů
        LockBuses_.EnterReadLock(); // subsystém autobusů
        Info.Buses = Buses_.Count();
        LockBuses_.ExitReadLock(); // subsystém autobusů
        LockDatabase_.ExitReadLock(); // zámek celé databáze
        return true;
    }
    /// <summary>
    /// Akce při spuštění serveru.
    /// </summary>
    /// Provádí inicializaci žurnálovacího mechanismu, který zajišťuje zachování dat i v případě 
    /// neočekávaného pádu aplikace.
    public virtual void OnStart() {
        if(Journal_ == null) {
            Journal_ = new WriteJournal(DataDir_, Constants.JOURNAL_LIMIT, this);
        }
    }
    /// <summary>
    /// Akce při zastavení serveru.
    /// </summary>
    /// Po zastavení komunikačních serverů se provede uložení paměťové databáze do perzistentní podoby 
    /// (do diskového souboru). Tím je zajištěno uchování dat i po ukončení celého serveru.
    public virtual void OnStop() {
        if(Journal_ != null) {
            Journal_.AskBackup();
            Journal_.OnStop();
            Journal_ = null;
        }
    }
    /// <summary>
    /// Adresář pro datové soubory.
    /// </summary>
    /// Nastavení adresáře pro soubory s interní zálohou databáze a žurnálu.
    /// <param name="DataDir">cesta k adresáři - prázdný řetězec pro aktuální adresář, jinak absolutní 
    /// nebo relativní cesta zakončená @e backslash na Windows nebo / na Unixových systémech.</param>
    public void SetDataDir(string DataDir) {
        DataDir_ = DataDir;
    }
    private Cities Cities_;
    private TreeDictionary<string, City> CityIndex_;
    private TreeDictionary<string, uint> CityAlias_;
    private Plans Plans_;
    private TreeDictionary<string, Plan> PlanIndex_;
    private Buses Buses_;
    private TreeSet<Bus> BusIndex_; // vlastni porovnavaci fce
    private ReaderWriterLockSlim LockCities_;
    private ReaderWriterLockSlim LockPlans_;
    private ReaderWriterLockSlim LockBuses_;
    internal ReaderWriterLockSlim LockDatabase_;
    private AutoIncrement Increment_;
    private void Init() {
        LockCities_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        LockPlans_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        LockBuses_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        LockDatabase_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        CityIndex_ = new TreeDictionary<string, City>(); // index měst
        PlanIndex_ = new TreeDictionary<string, Plan>(); // index plánů
        BusIndex_ = new TreeSet<Bus>(new BusLess()); // index autobusů
        Journal_ = null;
        Increment_ = new AutoIncrement();
    }
    private uint FindCityNoLock(string Name) {
        City city;

        CityIndex_.Find(ref Name, out city); // hledej v indexu dle jména
        if(city != null)
            return city.Id();
        else {
            uint Id;
            CityAlias_.Find(ref Name, out Id); // jinak prohledáme ještě aliasy
            return Id; // pokud se to nenajde, tak se vrati 0
        }
    }
    private bool CityInfoNoLock(uint City, out CityInfo Info, out uint LinesOut, out uint LinesIn) {
        City city;

        city = Cities_.Find(City); // nalezení dle čísla
        if(city == null) {
            Info = null;
            LinesIn = 0;
            LinesOut = 0;
            return false;
        }
        else {
            //if(Info != null)
                city.GetInfo(out Info); // konstatní informace
            //if(LinesOut != null)
                LinesOut = city.LinesOut(); // jako počáteční stanice linky
            //if(LinesIn != null)
                LinesIn = city.LinesIn(); // jako koncová stanice linky
            return true;
        }
    }
    private uint FindBusPlanNoLock(string Name) {
        Plan plan;

        PlanIndex_.Find(ref Name, out plan); // hledej v indexu dle jména
        if(plan == null) {
            return 0;
        }
        else {
            return plan.Id();
        }
    }
    private bool BusPlanInfoNoLock(uint Plan, out PlanInfo Info, out uint Buses, out string Data) {
        Plan plan;

        plan = Plans_.Find(Plan); // nalezení dle čísla
        if(plan == null) {
            Info = null;
            Buses = 0;
            Data = null;
            return false;
        }
        else {
            //if(Info != null)
                plan.GetInfo(out Info); // konstatní informace
            //if(Buses != null)
                Buses = plan.Buses(); // počet autobusů s tímto plánem
            //if(Data != null)
                plan.Data(out Data); // binární data
            return true;
        }
    }
    private uint FindBusNoLock(uint From, uint To, DateTime LeaveTime) {
        BusInfo Info = new global::BusInfo();
        Bus bus;
        Bus result;

        Info.Id = 0; // není využito
        Info.CityFrom = From;
        Info.CityTo = To;
        Info.LeaveTime = LeaveTime;
        Info.Line = 0; // není využito
        Info.Plan = 0; // není využito
        bus = new Bus(Info, 0, 0);

        // hledání v indexu dle stanic a času odjezdu
        if(BusIndex_.TryWeakSuccessor(bus, out result)) { // >= požadované hodnotě
            if(result.CityFrom() != From || result.CityTo() != To)
                return 0; // počáteční a cílová stanice musí souhlasit
            else
                return result.Id();
        }
        else {
            return 0;
        }
    }
    private string DataDir_;
    private WriteJournal Journal_;
    internal bool BackupNoLock(System.IO.Stream stream) {
        bool result = true;
        // výstup v chráněném bloku - vyvolání výjimky při selhání kterékoliv podoperace
        try {
            Backup backup = new global::Backup(stream);
            // hlavička
            backup.Header(BkHeader.DatObject.Database);
            backup.Unsigned(Increment_.Next()); // počítadlo (jedna vynechaná hodnota nevadí, podstatná je jedinečnost)
            // města
            Cities_.Backup(stream);
            // aliasy měst
            backup.Header(BkHeader.DatObject.Aliases);
            backup.Unsigned((uint)CityAlias_.Count); // počet položek
            foreach(var alias in CityAlias_) {
                backup.Header(BkHeader.DatObject.Alias);
                backup.String(alias.Key);
                backup.Unsigned(alias.Value);
            }
            // plány
            Plans_.Backup(stream);
            // autobusy
            Buses_.Backup(stream);
            // synchronizace proudu - vyprázdnění výstupního buferu
            backup.Header(BkHeader.DatObject.End);
            stream.Flush();
        }
        catch(Exception) {
            result = false;
        }
        return result;
    }
};

