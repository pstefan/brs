﻿using System;
using System.Net.Sockets;


/// <summary>
/// Pomocná třída pro komunikaci. Serverová i klientská aplikace má třídy, které jsou potomky této a které dodají položky <paramref name="Reader_"/> a <paramref name="Writer_"/>.
/// </summary>
class NetworkConnection {
    /// <summary>
    /// Poslaní příkazu. Interně se převádí na <c>int</c>.
    /// </summary>
    /// <param name="Cmd">Příkaz pro databázi.</param>
    public bool Write(QueryCmd Cmd) {
        Writer_.Write((uint)Cmd);
        Writer_.Flush();
        return true;
    }
    /// <summary>
    /// Poslaní řetězce. Nejprve se pošle délka řetězce jako <c>uint</c>, poté následuje pole znaků řetězce v kódování UTF-8.
    /// </summary>
    /// <param name="Text">Text.</param>
    public bool Write(string Text) {
        byte[] str = System.Text.Encoding.UTF8.GetBytes(Text);
        uint length = (uint)str.Length;
        Writer_.Write(length);
        Writer_.Write(str);
        Writer_.Flush();
        return true;
    }
    /// <summary>
    /// Poslání neznaménkového čísla.
    /// </summary>
    /// <param name="Number">Číslo.</param>
    public bool Write(uint Number) {
        Writer_.Write(Number);
        Writer_.Flush();
        return true;
    }
    /// <summary>
    /// Poslaní času a datumu. Posílá se v binární reprezentaci získané metodou <c>ToBinary()</c>.
    /// </summary>
    /// <param name="Time">Čas.</param>
    public bool Write(DateTime Time) {
        Writer_.Write(Time.ToBinary());
        Writer_.Flush();
        return true;
    }
    /// <summary>
    /// Poslání logické hodnoty.
    /// </summary>
    /// <param name="Bool">Logická hodnota.</param>
    public bool Write(bool Bool) {
        Writer_.Write(Bool);
        Writer_.Flush();
        return true;
    }
    /// <summary>
    /// Přečtení příkazu.
    /// </summary>
    /// <param name="Cmd">Příkaz .</param>
    public bool Read(out QueryCmd Cmd) {
        Cmd = (QueryCmd)Reader_.ReadUInt32();
        return true;
    }
    /// <summary>
    /// Přečtení řetězce. Probíhá inverzním způsobem k posílání řetězce.
    /// </summary>
    /// <param name="Text">Text.</param>
    public bool Read(out string Text) {
        uint length = Reader_.ReadUInt32();
        byte[] str = Reader_.ReadBytes((int)length);
        Text = System.Text.Encoding.UTF8.GetString(str);
        return true;
    }
    /// <summary>
    /// Přečtení neznaménkového čísla.
    /// </summary>
    /// <param name="Number">Číslo.</param>
    public bool Read(out uint Number) {
        Number = Reader_.ReadUInt32();
        return true;
    }
    /// <summary>
    /// Přečtení času a datumu. Načte se <c>Int64</c>, který se převede na typ <c>DateTime</c> metodou <c>FromBinary()</c>.
    /// </summary>
    /// <param name="Time">Čas.</param>
    public bool Read(out DateTime Time) {
        Time = DateTime.FromBinary(Reader_.ReadInt64());
        return true;
    }
    /// <summary>
    /// Přečtení logické hodnoty.
    /// </summary>
    /// <param name="Bool">Logická hodnota.</param>
    public bool Read(out bool Bool) {
        Bool = Reader_.ReadBoolean();
        return true;
    }
    /// <summary>
    /// Binární prou pro čtení.
    /// </summary>
    protected System.IO.BinaryReader Reader_;
    /// <summary>
    /// Binární proud pro zápis.
    /// </summary>
    protected System.IO.BinaryWriter Writer_;
};
