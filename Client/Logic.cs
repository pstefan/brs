﻿using System;
using System.Collections.Generic;

/// <summary>
/// Třída obsahující informace k vypsání autobusu.
/// </summary>
class BusPrint {
    //!ID autobusu
    public uint Id;
    //!číslo linky autobusu
    public uint Line;
    //!název výchozího města
    public string CityFrom;
    //!název cílového města
    public string CityTo;
    //!čas odjezdu v UNIX timestamp
    public DateTime LeaveTime;
    //!plán autobusu
    public string BusPlan;
}

/// <summary>
/// Kódy chybových hlášení.
/// </summary>
enum Error {NOERR, CITYFROM, CITYTO, LEAVETIME, CTFROMNFOUND, CTTONFOUND, CTNFOUND, TIMEFORMAT, BUSNFOUND,
    NBUSESFOUND, NUMSEATS, NBUSSET, NUSERSET, NFREESEATS, WRITESEATS, NRESERVE, NUNLOCK, WRITEBLINE, WRITEBPLAN,
    PLANNFOUND, WRITEBID, NDELBUS, WRCITY, TOOMANYARGS, NADDCITY, NDELCITY, NADDALIAS, WRALIAS, WRBUSPLAN, WRSEATSCOUNT,
    NADDPLAN, NDELPLAN, NFILE, NADDBUS, WRONGBID, FIRSTFIND, WRUSERID, NTYPESET, WRARGUMENT, FILEREADFAIL, BUSDISABLED,
    USEDBUS, NDISBUS};

/// <summary>
/// Třída na převod uživatelských dotazů na dotazy pro databázi.
/// </summary>
/// Třída zajišťuje převod dotazu uživatele na dotazy pro databázi a případné nastavení chybové hodnoty pro všechny nepovolené vstupy.
/// 
/// Chybové kódy po každém příkazu si můžeme zjistit voláním @ref GetLastError().
/// 
/// Pamatuje si předtím uživatelem zadané hodnoty a tím umožňuje používat jednodušší a snáze zapamatovatelné příkazy.
class Logic {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="Query">Ukazatel ne nějakou třídu poskytující rozhraní IQuery, pomocí které bude probíhat komunikace s databází.</param>
    public Logic(IQuery Query) {
        CurLeaveTime_ = DateTime.MinValue;
        CurBus_ = 0;
        CurCityFrom_ = 0;
        CurCityTo_ = 0;
        CurUserId_ = 0;
        NexLeaveTime_ = DateTime.MinValue;
        LastError_ = Error.NOERR;
        Database_ = Query;
    }
    /// <summary>
    /// Inicializace, mj. přidělení unikátního IP novému zákazníkovi.
    /// </summary>
    public void Init() {
        Clear();
        CurUserId_ = Database_.NewCustomer();
    }
    /// <summary>
    /// Vymazání aktuálních dat.
    /// </summary>
    /// Vymaže aktuální hodnoty jako ID aktuálního uživatele, aktuálně vybraný autobus, výchozí a cílové město z posledního hledání a čas odjezdu posledního nalezeného autobusu.
    public void Clear() {
        LastError_ = Error.NOERR;
        CurLeaveTime_ = DateTime.MinValue;
        CurBus_ = 0;
        CurCityFrom_ = 0;
        CurCityTo_ = 0;
        CurUserId_ = 0;
    }
    /// <summary>
    /// Nalezení vyhovujících autobusů.
    /// </summary>
    /// <param name="Buses">fronta nalezených autobusů k vypsání.</param>
    /// <param name="CityFrom">řetězec s názvem výchozího města.</param>
    /// <param name="CityTo">řetězec s názvem cílového města.</param>
    /// <param name="LeaveTime">řetězec s časem, od kterého dál se má v databáti vyhledávat.</param>
    /// <returns>@e true při úspěchu, @e false jinak</returns>
    public bool Find(out Queue<BusPrint> Buses, string CityFrom, string CityTo, string LeaveTime) {
        Buses = null;
        LastError_ = Error.NOERR;
        CurCityFrom_ = Database_.FindCity(CityFrom);
        if(CurCityFrom_ == 0) {
            LastError_ = Error.CITYFROM;
            return false;
        }
        CurCityTo_ = Database_.FindCity(CityTo);
        if(CurCityTo_ == 0) {
            LastError_ = Error.CITYTO;
            return false;
        }
        CurLeaveTime_ = ConvertTime(LeaveTime);
        if(CurLeaveTime_ == DateTime.MinValue) {
            LastError_ = Error.LEAVETIME;
            return false;
        }
        NexLeaveTime_ = CurLeaveTime_;
        if(Next(out Buses)) {
            return true;
        }
        else {
            LastError_ = Error.NBUSESFOUND;
            return false;
        }
    }
    /// <summary>
    /// Vypsání dalších autobusů.
    /// </summary>
    /// Vypsání dalších autobusů. Nutná podmínka je, aby byl před tímto použit příkaz "find".
    /// <param name="Buses">fronta nalezených autobusů k vypsání.</param>
    /// <returns>@e true při úspěchu, @e false jinak</returns>
    public bool Next(out Queue<BusPrint> Buses) {
        uint CurBus;
        BusInfo busInfo;
        CityInfo cityInfo;
        PlanInfo planInfo;
        Buses = new Queue<BusPrint>();

        LastError_ = Error.NOERR;
        if(NexLeaveTime_ == DateTime.MinValue || CurCityFrom_ == 0 || CurCityTo_ == 0) {
            LastError_ = Error.FIRSTFIND;
            return false;
        }
        for(uint i = 0; i < PrintCount; i++) {
            CurBus = Database_.FindBus(CurCityFrom_, CurCityTo_, NexLeaveTime_);
            if(CurBus != 0) {
                bool a; uint b; Queue<uint> c; string d;
                BusPrint busPrint = new BusPrint();
                Database_.BusInfo(CurBus, out busInfo, out a, out b, out c);
                busPrint.Id = busInfo.Id;
                busPrint.LeaveTime = busInfo.LeaveTime;
                busPrint.Line = busInfo.Line;
                Database_.CityInfo(busInfo.CityFrom, out cityInfo, out b, out b);
                busPrint.CityFrom = cityInfo.Name;
                Database_.CityInfo(busInfo.CityTo, out cityInfo, out b, out b);
                busPrint.CityTo = cityInfo.Name;
                Database_.BusPlanInfo(busInfo.Plan, out planInfo, out b, out d);
                busPrint.BusPlan = planInfo.Name;
                Buses.Enqueue(busPrint);
                NexLeaveTime_ = busInfo.LeaveTime.AddMinutes(1);
                if(i == 0)
                    CurLeaveTime_ = busInfo.LeaveTime;
            }
            else {
                if(Buses.Count == 0) {
                    LastError_ = Error.NBUSESFOUND;
                    return false;
                }
                break;
            }
        }
        return true;
    }
    /// <summary>
    /// Nastaví aktuální autobus.
    /// </summary>
    /// <returns>@e true při úspěchu, @e false jinak.</returns>
    /// <param name="BusId">ID autobusu, kterého chceme nastavit jako aktivního.</param>
    /// <param name="FreeSeats">seznam volných sedadel nastaveného autobusu.</param>
    /// <param name="Plan">řetězec s plánkem autobusu v textové formě.</param>
    /// <param name="Bus">informace o lince.</param>
    /// <param name="NumberSeats">celkový počet sedadel v autobusu.</param>
    public bool SetBus(uint BusId, out Queue<uint> FreeSeats, out string Plan, out BusPrint Bus, out uint NumberSeats) {
        BusInfo busInfo = new BusInfo();
        CityInfo cityInfo = new CityInfo();
        PlanInfo planInfo = new PlanInfo();
        bool Enable;
        uint NumSeats;
        LastError_ = Error.NOERR;
        Bus = new BusPrint();
        Plan = null;
        NumberSeats = 0;

        if(Database_.BusInfo(BusId, out busInfo, out Enable, out NumSeats, out FreeSeats)) {
            NumberSeats = (uint)NumSeats;
            if(!(bool)Enable) {
                LastError_ = Error.BUSDISABLED;
                return false;
            }
            uint a;
            CurBus_ = BusId;
            Database_.BusPlanInfo(busInfo.Plan, out planInfo, out a, out Plan);
            Bus.Id = CurBus_;
            Bus.Line = busInfo.Line;
            Database_.CityInfo(busInfo.CityFrom, out cityInfo, out a, out a);
            Bus.CityFrom = cityInfo.Name;
            Database_.CityInfo(busInfo.CityTo, out cityInfo, out a, out a);
            Bus.CityTo = cityInfo.Name;
            Bus.LeaveTime = busInfo.LeaveTime;
            Bus.BusPlan = planInfo.Name;
            return true;
        }
        else {
            LastError_ = Error.WRONGBID;
            return false;
        }
    }
    /// <summary>
    /// Dočasná rezervace.
    /// </summary>
    /// Zarezervuje zadaný počet sedadel jen na určitou dobu, poté budou automaticky uvolněna.
    /// <returns>@e true pokud se podařilo rezervovat všechna sedadla, @e false jinak.</returns>
    /// <param name="Seats">seznam sedadel, které se podařilo rezervovat.</param>
    /// <param name="SeatsCount">počet sedadel k rezervaci.</param>
    public bool ReserveAsk(out Queue<uint> Seats, uint SeatsCount) {
        LastError_ = Error.NOERR;
        Seats = new Queue<uint>();
        if(CurBus_ == 0) {
            LastError_ = Error.NBUSSET;
            return false;
        }
        if(CurUserId_ == 0) {
            LastError_ = Error.NUSERSET;
            return false;
        }
        if(SeatsCount == 0) {
            LastError_ = Error.WRSEATSCOUNT;
            return false;
        }
        do {
            BusInfo a; uint b; bool c;
            Database_.BusInfo(CurBus_, out a, out c, out b, out Seats);
            uint NumSeats = (uint)(Seats.Count);
            if(NumSeats < SeatsCount) {
                Seats.Clear();
                LastError_ = Error.NFREESEATS;
                return false; // neni tolik volných míst
            }
            else {
                NumSeats -= SeatsCount;
                while(NumSeats-- != 0)
                    Seats.Dequeue();
            }
        } while(!Database_.AskSeats(CurBus_, ref Seats, CurUserId_));
        return true;
    }
    /// <summary>
    /// Dočasná rezervace.
    /// </summary>
    /// Zarezervuje zadaná sedadla jen na určitou dobu, poté budou automaticky uvolněna.
    /// <returns>@e true pokud se podařilo rezervovat všechna sedadla, @e false jinak.</returns>
    /// <param name="Seats">seznam sedadel k rezervaci. Sedadla, která se nepovedlo rezervovat, budou z tohoto seznamu odstraněna.</param>
    public bool ReserveAsk(ref Queue<uint> Seats) {
        LastError_ = Error.NOERR;
        if(CurBus_ == 0) {
            LastError_ = Error.NBUSSET;
            return false;
        }
        if(CurUserId_ == 0) {
            LastError_ = Error.NUSERSET;
            return false;
        }
        if(Seats.Count == 0) {
            LastError_ = Error.WRITESEATS;
            return false;
        }
        if(!Database_.AskSeats(CurBus_, ref Seats, CurUserId_)) {
            LastError_ = Error.NRESERVE;
            return false;
        }
        else
            return true;
    }
    /// <summary>
    /// Konečná rezervace.
    /// </summary>
    /// Definitivně zarezervuje zadaná sedadla.
    /// <returns>@e true pokud se podařilo rezervovat všechna sedadla, @e false jinak.</returns>
    /// <param name="Seats">seznam sedadel k rezervaci. Sedadla, která se nepovedlo rezervovat, budou z tohoto seznamu odstraněna.</param>
    /// <param name="Id">ID uživatele, který provedl rezervaci.</param>
    /// <param name="Bus">data o autobusu, ve kterém byla provedene rezervace.</param>
    public bool ReserveForever(ref Queue<uint> Seats, out uint Id, out BusPrint Bus) {
        BusInfo busInfo;
        CityInfo cityInfo;
        Id = 0;

        Bus = new BusPrint();
        LastError_ = Error.NOERR;
        if(CurBus_ == 0 || CurUserId_ == 0)
            return false;
        Id = CurUserId_;

        bool a; uint b; Queue<uint> c;
        Bus.Id = CurBus_;
        Database_.BusInfo(CurBus_, out busInfo, out a, out b, out c);
        Bus.Line = busInfo.Line;
        Database_.CityInfo(busInfo.CityFrom, out cityInfo, out b, out b);
        Bus.CityFrom = cityInfo.Name;
        Database_.CityInfo(busInfo.CityTo, out cityInfo, out b, out b);
        Bus.CityTo = cityInfo.Name;
        Bus.LeaveTime = busInfo.LeaveTime;

        bool Result = Database_.LockSeats(CurBus_, ref Seats, CurUserId_);
        CurUserId_ = Database_.NewCustomer(); // nové číslo zákazníka
        return Result;
    }
    /// <summary>
    /// Uvolnění sedadel.
    /// </summary>
    /// Provede uvolnění zadaných sedadel v aktuálním autobusu s aktuálním ID uživatele. Užitečné zejména pro uvolnění dočasně rezervovaných sedadel při nepotvrzení rezervace.
    /// <returns>@e true pokud se podařilo uvolnit sedadla, @e false jinak.</returns>
    /// <param name="UserId">ID uživatele, který chce zrušit svou rezervaci.</param>
    /// <param name="Seats">seznam uvolněných sedadel.</param>
    public bool UnlockAll(uint UserId, out Queue<uint> Seats) {
        Seats = null;
        LastError_ = Error.NOERR;
        if(UserId == 0) {
            LastError_ = Error.WRUSERID;
            return false;
        }
        if(CurBus_ == 0) {
            LastError_ = Error.NBUSSET;
            return false;
        }
        uint a;
        ReservedSeats(CurBus_, out Seats, out a);
        Database_.UnlockSeats(CurBus_, ref Seats, UserId);
        return true;
    }
    /// <summary>
    /// Uvolnění sedadel.
    /// </summary>
    /// Provede uvolnění zadaných sedadel v aktuálním autobusu s aktuálním ID uživatele.
    /// <param name="Seats">seznam sedadel k uvolnění, po návratu uvolněná sedadla.</param>
    /// <returns>@e true pokud se podařilo uvolnit sedadla, @e false jinak.</returns>
    public bool Unlock(ref Queue<uint> Seats) {
        LastError_ = Error.NOERR;
        return Unlock(CurUserId_, ref Seats);
    }
    /// <summary>
    /// Uvolnění sedadel.
    /// </summary>
    /// Provede uvolnění zadaných sedadel v aktuálním autobusu se zadaným ID uživatele.
    /// <param name="UserId">ID uživatele, který chce zrušit svou rezervaci.</param>
    /// <param name="Seats">seznam sedadel k uvolnění, po návratu uvolněná sedadla.</param>
    /// <returns>@e true pokud se podařilo uvolnit sedadla, @e false jinak.</returns>
    public bool Unlock(uint UserId, ref Queue<uint> Seats) {
        LastError_ = Error.NOERR;
        if(UserId == 0) {
            LastError_ = Error.WRUSERID;
            return false;
        }
        if(CurBus_ == 0) {
            LastError_ = Error.NBUSSET;
            return false;
        }
        if(Database_.UnlockSeats(CurBus_, ref Seats, UserId))
            return true;
        else {
            LastError_ = Error.NUNLOCK;
            return false;
        }
    }
    /// <summary>
    /// Přidání nového města.
    /// </summary>
    /// Přidá nové město do databáze.
    /// <returns>ID nově přidaného města, v případě neúspěchu 0.</returns>
    /// <param name="CityName">název nově přidávaného města.</param>
    public uint AddCity(string CityName) {
        LastError_ = Error.NOERR;
        if(CityName == "") {
            LastError_ = Error.WRCITY;
            return 0;
        }
        return Database_.AddCity(CityName);
    }
    /// <summary>
    /// Smazání města.
    /// </summary>
    /// Smaže stávající město z databáze.
    /// <returns>@e true při úspěchu, @e false jinak.</returns>
    /// <param name="CityName">název města ke smazání.</param>
    public bool DelCity(string CityName) {
        LastError_ = Error.NOERR;
        if(CityName == "") {
            LastError_ = Error.WRCITY;
            return false;
        }
        if(Database_.DeleteCity(CityName))
            return true;
        else {
            LastError_ = Error.NDELCITY;
            return false;
        }
    }
    /// <summary>
    /// Přídání zkratky pro stávající město.
    /// </summary>
    /// <returns>@e true při úspěchu, @e false jinak.</returns>
    /// <param name="CityAlias">nová zkratka pro město.</param>
    /// <param name="CityName">jméno stávajícího města.</param>
    public bool AddCityAlias(string CityAlias, string CityName) {
        LastError_ = Error.NOERR;
        if(CityAlias == "") {
            LastError_ = Error.WRALIAS;
            return false;
        }
        if(CityName == "") {
            LastError_ = Error.WRCITY;
            return false;
        }
        uint Id = Database_.FindCity(CityName);
        if(Id == 0) {
            LastError_ = Error.CTNFOUND;
            return false;
        }
        return Database_.AddCityAlias(CityAlias, Id);
    }
    /// <summary>
    /// Přidání nového autobusu.
    /// </summary>
    /// Přidá nový autobus do databáze.
    /// <returns>ID nově přidaného autobusu, 0 v případě chyby.</returns>
    /// <param name="Line">číslo linky autobusu.</param>
    /// <param name="CityFrom">název výchozího města.</param>
    /// <param name="CityTo">název cílového města.</param>
    /// <param name="LeaveTime">řetězec s časem odjezdu autobusu.</param>
    /// <param name="Plan">název plánu/typu autobusu.</param>
    public uint AddBus(uint Line, string CityFrom, string CityTo, string LeaveTime, string Plan) {
        BusInfo Bus = new BusInfo();

        LastError_ = Error.NOERR;
        if(Line == 0) {
            LastError_ = Error.WRITEBLINE;
            return 0;
        }
        if(CityFrom == "") {
            LastError_ = Error.CITYFROM;
            return 0;
        }
        if(CityTo == "") {
            LastError_ = Error.CITYTO;
            return 0;
        }
        if(LeaveTime == "") {
            LastError_ = Error.LEAVETIME;
            return 0;
        }
        if(Plan == "") {
            LastError_ = Error.WRBUSPLAN;
            return 0;
        }
        uint UiCityFrom = Database_.FindCity(CityFrom);
        if(UiCityFrom == 0) {
            LastError_ = Error.CTFROMNFOUND;
            return 0;
        }
        uint UiCityTo = Database_.FindCity(CityTo);
        if(UiCityTo == 0) {
            LastError_ = Error.CTTONFOUND;
            return 0;
        }
        uint UiPlan = Database_.FindBusPlan(Plan);
        if(UiPlan == 0) {
            LastError_ = Error.PLANNFOUND;
            return 0;
        }
        DateTime DLeaveTime = ConvertTime(LeaveTime);
        if(DLeaveTime == DateTime.MinValue) {
            LastError_ = Error.TIMEFORMAT;
            return 0;
        }
        Bus.LeaveTime = DLeaveTime;
        Bus.CityFrom = UiCityFrom;
        Bus.CityTo = UiCityTo;
        Bus.Plan = UiPlan;
        Bus.Line = Line;
        uint UiBus = Database_.AddBus(Bus);
        if(UiBus == 0)
            LastError_ = Error.NADDBUS;
        return UiBus;
    }
    /// <summary>
    /// Smazání autobusu.
    /// </summary>
    /// Smaže autobus podle jeho ID.
    /// <returns>@e true při úspěchu, @e false jinak.</returns>
    /// <param name="BusId">ID autobusu určeného ke smazání.</param>
    public bool DelBus(uint BusId) {
        LastError_ = Error.NOERR;
        if(BusId == 0) {
            LastError_ = Error.WRITEBID;
            return false;
        }
        bool Enabled;
        BusInfo Bus;
        uint a; Queue<uint> b;
        Database_.BusInfo(BusId, out Bus, out Enabled, out a, out b);
        if((bool)Enabled && Bus.LeaveTime > DateTime.Now) {
            LastError_ = Error.USEDBUS;
            return false;
        }
        if(Database_.DeleteBus(BusId))
            return true;
        else {
            LastError_ = Error.NDELBUS;
            return false;
        }
    }
    /// <summary>
    /// Přidání nového plánu autobusu.
    /// </summary>
    /// Přidá do databáze nový plán či typ autobusu.
    /// <returns>ID přidaného plánu, 0 v případě chyby.</returns>
    /// <param name="Name">název plánu autobusu.</param>
    /// <param name="Seats">počet sedadel v tomto plánu.</param>
    /// <param name="File">název textového souboru obsahujícího plán v textové formě.</param>
    public uint AddBusPlan(string Name, uint Seats, string File) {
        LastError_ = Error.NOERR;
        if(Name == "") {
            LastError_ = Error.WRITEBPLAN;
            return 0;
        }
        if(Seats == 0) {
            LastError_ = Error.WRSEATSCOUNT;
            return 0;
        }
        if(File == "") {
            LastError_ = Error.NFILE;
            return 0;
        }
        System.IO.StreamReader stream;
        try {
            stream = new System.IO.StreamReader(File);
        }
        catch(Exception) {
            LastError_ = Error.NFILE;
            return 0;
        }

        string sFile;
        sFile = stream.ReadToEnd();
        stream.Close();
        uint Plan = Database_.AddBusPlan(Name, Seats, sFile);
        if(Plan == 0)
            LastError_ = Error.NADDPLAN;
        return Plan;

    }
    /// <summary>
    /// Smazání plánu autobusu.
    /// </summary>
    /// Smaže plán autobusu z databáze.
    /// <returns>@e true při úspěchu, @e false jinak.</returns>
    /// <param name="Name">jméno plánu autobusu ke smazání.</param>
    public bool DelBusPlan(string Name) {
        LastError_ = Error.NOERR;
        if(Name == "") {
            LastError_ = Error.WRBUSPLAN;
            return false;
        }
        if(Database_.DeleteBusPlan(Name)) {
            return true;
        }
        else {
            LastError_ = Error.NDELPLAN;
            return false;
        }
    }
    /// <summary>
    /// Uzamknutí autobusu.
    /// </summary>
    /// Uzamkne autobus pro další rezervace a vypíše seznam obsazených sedadel.
    /// <returns>@e true při úspěchu, @e false jinak</returns>
    /// <param name="BusId">číslo autobusu.</param>
    /// <param name="QReservedSeats">fronta obsazených sedadel.</param>
    /// <param name="Plan">plánek autobusu.</param>
    /// <param name="BusPrint">informace o autobusu.</param>
    /// <param name="Seats">počet sedadel autobusu.</param>
    public bool Close(uint BusId, out Queue<uint> QReservedSeats, out string Plan, out BusPrint BusPrint, out uint Seats) {
        LastError_ = Error.NOERR;
        QReservedSeats = new Queue<uint>();
        Plan = null;
        BusPrint = new BusPrint();
        Seats = 0;
        if(!Database_.DisableBus(BusId)) {
            LastError_ = Error.NDISBUS;
            return false;
        }
        ReservedSeats(BusId, out QReservedSeats, out Seats);
        BusInfo busInfo;
        CityInfo cityInfo;
        PlanInfo planInfo;
        bool Enable;
        uint NumberSeats;
        LastError_ = Error.NOERR;
        uint a; Queue<uint> b;
        if(Database_.BusInfo(BusId, out busInfo, out Enable, out NumberSeats, out b)) {
            CurBus_ = BusId;
            Database_.BusPlanInfo(busInfo.Plan, out planInfo, out a, out Plan);

            BusPrint.Id = CurBus_;
            BusPrint.Line = busInfo.Line;
            Database_.CityInfo(busInfo.CityFrom, out cityInfo, out a, out a);
            BusPrint.CityFrom = cityInfo.Name;
            Database_.CityInfo(busInfo.CityTo, out cityInfo, out a, out a);
            BusPrint.CityTo = cityInfo.Name;
            BusPrint.LeaveTime = busInfo.LeaveTime;
            BusPrint.BusPlan = planInfo.Name;

            return true;
        }
        return true;
    }
    /// <summary>
    /// Zjištění případné chyby v poslední funkci.
    /// </summary>
    /// Zjistí stav, se kterým skončila posledně volaná funkce.
    /// <returns>chybový stav z výčtového typu Error.</returns>
    public Error GetLastError() {
        return LastError_;
    }
    /// <summary>
    /// Smazání starých autobusů.
    /// </summary>
    /// Smaže autobusy, které již odjely.
    /// <returns>počet smazaných autobusů.</returns>
    public uint ClearBus() {
        uint Counter = 0;
        uint Bus = 0;
        DateTime Now = DateTime.Now;
        BusInfo Info;
        while((Bus = Database_.NextBus(Bus)) != 0) {
            bool a; uint b; Queue<uint> c;
            Database_.BusInfo(Bus, out Info, out a, out b, out c);
            if(Info.LeaveTime < Now)
            if(Database_.DeleteBus(Bus))
                Counter++;
        }
        LastError_ = Error.NOERR;
        return Counter;
    }
    /// <summary>
    /// Smazání nepoužívaných měst.
    /// </summary>
    /// Smaže města, do kterých ani z kterých nejezdí žádné autobusy.
    /// <returns>počet smazaných měst.</returns>
    public uint ClearCity() {
        uint Counter = 0;
        uint Out, In;
        CityInfo Info;
        uint City = 0;
        while((City = Database_.NextCity(City)) != 0) {
            Database_.CityInfo(City, out Info, out Out, out In);
            if(Out == 0 && In == 0)
            if(Database_.DeleteCity(Info.Name))
                Counter++;
        }
        LastError_ = Error.NOERR;
        return Counter;
    }
    /// <summary>
    /// Smazání nepoužívaných plánů autobusů.
    /// </summary>
    /// Smaže neyužívané plány autobusů z databáze.
    /// <returns>počet smazaných plánů.</returns>
    public uint ClearPlan() {
        uint Counter = 0;
        uint Buses;
        string s;
        PlanInfo Info;
        uint Plan = 0;
        while((Plan = Database_.NextPlan(Plan)) != 0) {
            Database_.BusPlanInfo(Plan, out Info, out Buses, out s);
            if(Buses == 0)
            if(Database_.DeleteBusPlan(Info.Name))
                Counter++;
        }
        LastError_ = Error.NOERR;
        return Counter;
    }
    /// <summary>
    /// Vypsání existujících měst.
    /// </summary>
    /// Vypíše existující města z databáze.
    /// <param name="input">vstupní sekvence, podle které se hledají podobná města.</param>
    /// <param name="Cities">seznam nalezených měst. Názvy měst mají druhý string prázdný, aliasy jsou v druhém stringu a v prvním je i opravdový název města.</param>
    public void City(string input, out List<KeyValuePair<string, string>> Cities) {
        Cities = new List<KeyValuePair<string, string>>();
        LastError_ = Error.NOERR;
        string temp = input;
        string answer;
        string alias = "";
        bool Ret;
        if(input == null) {
            Ret = Database_.ListCity("", out answer);
            while(Ret) {
                Cities.Add(new KeyValuePair<string, string>(answer, alias));
                temp = answer + " ";
                Ret = Database_.ListCity(temp, out answer);
            }
            temp = "";
            Ret = Database_.ListAlias(temp, out alias, out answer);
            while(Ret) {
                Cities.Add(new KeyValuePair<string, string>(answer, alias));
                temp = alias + " ";
                Ret = Database_.ListAlias(temp, out alias, out answer);
            }
        }
        else {
            Ret = Database_.ListCity(temp, out answer);
            while(answer != null && answer.Length >= input.Length && answer.Substring(0, input.Length) == input && Ret) {
                Cities.Add(new KeyValuePair<string, string>(answer, alias));
                temp = answer + " ";
                Ret = Database_.ListCity(temp, out answer);
            }
            temp = input;
            Ret = Database_.ListAlias(temp, out alias, out answer);
            while(alias != null && alias.Length >= input.Length && alias.Substring(0, input.Length) == input && Ret) {
                Cities.Add(new KeyValuePair<string, string>(answer, alias));
                temp = alias + " ";
                Ret = Database_.ListAlias(temp, out alias, out answer);
            }
        }
    }
    private const uint PrintCount = 3;
    private IQuery Database_;
    private uint CurBus_;
    private uint CurUserId_;
    private uint CurCityFrom_;
    private uint CurCityTo_;
    private DateTime CurLeaveTime_;
    private DateTime NexLeaveTime_;
    private bool ReservedSeats(uint Bus, out Queue<uint> Seats, out uint NumSeats) {
        Seats = new Queue<uint>();
        Queue<uint> FreeSeats;
        NumSeats = 0;

        bool a; BusInfo b;
        if(Database_.BusInfo(Bus, out b, out a, out NumSeats, out FreeSeats)) {
            Seats.Clear();
            var it = FreeSeats.GetEnumerator();
            bool end = !it.MoveNext();
            for(uint i = 1; i <= NumSeats; i++) {
                if(!end) {
                    if(i < it.Current)
                        Seats.Enqueue(i);
                    else if (i == it.Current)
                        end = !it.MoveNext();
                }
                else
                    Seats.Enqueue(i);
            }

            return true;
        }
        return false;
    }
    private DateTime ConvertTime(string Time) {
        //formát je 13:15-2.10.2013 nebo jen 13:15
        DateTime result;
        if(DateTime.TryParseExact(Time, "H:mm-d.M.yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out result)) {
            return result;
        }
        else {
            if(DateTime.TryParseExact(Time, "H:mm", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out result)) {
                return result;
            }
            else {
                return DateTime.MinValue;
            }
        }
    }
    private Error LastError_;
}

