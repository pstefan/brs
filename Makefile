#CC=/opt/mono/bin/dmcs
CC=dmcs
#MONO=/opt/mono/bin/mono
MONO=mono
OPTIONS=-optimize+ -target:exe -pkg:dotnet
DESTDIR=bin
DOC_DIR=doc

C5_LIB_NAME=C5.dll
C5_LIB_DOWNLOAD_URI=http://files.petrstefan.tk/WWW/$(C5_LIB_NAME)
SERVER_LIBS=-lib:$(DESTDIR) -r:$(C5_LIB_NAME)

DATABASE_DIR=Server/Database
DATABASE_SOURCE=Backup.cs Bus.cs City.cs Journal.cs MemoryDB.cs Plan.cs Seat.cs
SERVER_DIR=Server
SERVER_SOURCE=Program.cs Admin.cs ServerApp.cs ServerConnection.cs ServerProcess.cs
CLIENT_DIR=Client
CLIENT_SOURCE=ClientApp.cs ClientConnection.cs ClientProcess.cs Graphics.cs Logic.cs Program.cs
COMMON_DIR=Common
COMMON_SOURCE=AutoIncrement.cs NetworkConnection.cs Query.cs Constants.cs

DATABASE_FILES=$(patsubst %,$(DATABASE_DIR)/%,$(DATABASE_SOURCE))
SERVER_FILES=$(patsubst %,$(SERVER_DIR)/%,$(SERVER_SOURCE))
CLIENT_FILES=$(patsubst %,$(CLIENT_DIR)/%,$(CLIENT_SOURCE))
COMMON_FILES=$(patsubst %,$(COMMON_DIR)/%,$(COMMON_SOURCE))

all: server client

server: $(DESTDIR)/BRS\#_server | $(DESTDIR)/.

client: $(DESTDIR)/BRS\#_client | $(DESTDIR)/.

$(DESTDIR)/.:
	mkdir -p $(DESTDIR)

$(DESTDIR)/$(C5_LIB_NAME): | $(DESTDIR)/.
	# wget -P $(DESTDIR) $(C5_LIB_DOWNLOAD_URI)
	cp Utils/$(C5_LIB_NAME) $(DESTDIR)

$(DESTDIR)/BRS\#_server: $(DATABASE_FILES) $(SERVER_FILES) $(COMMON_FILES) | $(DESTDIR)/$(C5_LIB_NAME)
	$(CC) $(OPTIONS) $(SERVER_LIBS) $(DATABASE_FILES) $(SERVER_FILES) $(COMMON_FILES) -out:$@

$(DESTDIR)/BRS\#_client: $(CLIENT_FILES) $(COMMON_FILES)
	$(CC) $(OPTIONS) $(CLIENT_FILES) $(COMMON_FILES) -out:$@

prepare:
	cp Utils/*.conf $(DESTDIR)
	cp Utils/*.txt $(DESTDIR)
	
start:
	cd $(DESTDIR) && \
	gnome-terminal -e '$(MONO) BRS#_server' && \
	sleep 1 && \
	$(MONO) BRS#_client < fill_db.txt > /dev/null && \
	gnome-terminal -e '$(MONO) BRS#_client'

.PHONY: clean

clean:
	rm -rf $(DESTDIR)/

.PHONY: size

size:
	@find . -name '*.cs' -exec du -b '{}' \; | cut -f1 | awk '{s+=$$1} END {s/=1024; print "Source total size: ",s,"KiB";}'

.PHONY: doc

doc:
	doxygen

