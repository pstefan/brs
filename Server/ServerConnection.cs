﻿using System;
using System.Net.Sockets;

/// <summary>
/// serverová část pro komunikaci pomocí TCP protokolu.
/// </summary>
class ServerConnection : NetworkConnection {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="Client">objekt typu TcpClient získaný metodou @c AcceptTcpClient().</param>
    public ServerConnection(TcpClient Client) {
        Client_ = Client;
        Reader_ = new System.IO.BinaryReader(Client_.GetStream());
        Writer_ = new System.IO.BinaryWriter(Client_.GetStream());
    }
    public void Close() {
        Client_.Close();
    }
    private TcpClient Client_;
}
