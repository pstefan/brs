using System;
using System.Collections.Generic;
using System.Threading;
using C5;

// Poznamka - hodnoty, ktere se v prubehu doby existence autobusu
//            nemeni, neni treba chranit zamkem

/// <summary>
/// Autobus.
/// </summary>
/// Třída obsahuje všechna data o jednom autobusu. Obsahuje konstatní data ve struktuře @ref BusInfo,
/// kontajner sedadel (instancí třídy @ref Seat) a další položky (jako zámek pro vícenásobný přístup
/// @c ReaderWriterLockSlim), které mohou v průběhu života třídy své hodnoty měnit. Každá instance 
/// má své unikátní číslo. Informace o počáteční a cílové stanici jsou uloženy ve struktuře BusInfo,
/// plán sedadel je určen šablonou typu @ref Plan.
///
/// @image html Bus.png "Schematické znázornění třídy Bus"
///
/// @note Některé metody pracují se seznamy, ve kterých vystupují čísla sedadel. Stejně jako ve skutečném 
/// autobusu jsou sedadla číslována od 1 (0 není platné číslo sedadla). Nicméně je jasné, že interně jsou
/// položky kontajneru sedadel indexovány od 0.
class Bus {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// Vytvoření nového autobusu, který obsahuje kontajner zadaného počtu sedadel.
    /// <param name="Info">struktura @ref BusInfo, která obsahuje data pro tento autobus.</param>
    /// <param name="Id">jedinečné ID tohoto autobusu.</param>
    /// <param name="Seats">počet sedadel.</param>
    public Bus(BusInfo Info, uint Id, uint Seats) {
        BusInfo_ = Info;
        SeatList_ = new List<Seat>((int)Seats);
        for(uint i = 0; i < Seats; i++) {
            SeatList_.Add(new Seat());
        }
        BusInfo_.Id = Id;
        Lock_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        Enabled_ = true;
        Seats_ = Seats;
    }
    /// <summary>
    /// Načtení z proudu.
    /// </summary>
    /// Třída je vytvořena načtením všech položek z proudu.
    /// <param name="stream">vstupní proud s binárními daty.</param>
    public Bus(System.IO.Stream stream) {
        Lock_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        SeatList_ = new List<Seat>();
        BusInfo_ = new BusInfo();
        Restore restore = new Restore(stream);
        restore.Header(BkHeader.DatObject.Bus);
        BusInfo_.Id = restore.Unsigned();
        BusInfo_.Line = restore.Unsigned();
        BusInfo_.CityFrom = restore.Unsigned();
        BusInfo_.CityTo = restore.Unsigned();
        BusInfo_.Plan = restore.Unsigned();
        BusInfo_.LeaveTime = restore.Time();
        Enabled_ = restore.Bool();
        Seats_ = restore.Unsigned();
        SeatList_.Capacity = (int)Seats_;
        for(uint i = 0; i < Seats_; i++) {
            SeatList_.Add(new Seat(stream));
        }
    }
    /// <summary>
    /// vrátí ID autobusu
    /// </summary>
    public uint Id() {
        return BusInfo_.Id;
    }
    /// <summary>
    /// vrátí ID počátečního města
    /// </summary>
    public uint CityFrom() {
        return BusInfo_.CityFrom;
    }
    /// <summary>
    /// vrátí ID cílového města
    /// </summary>
    public uint CityTo() {
        return BusInfo_.CityTo;
    }
    /// <summary>
    /// vrátí ID plánu autobusu
    /// </summary>
    public uint Plan() {
        return BusInfo_.Plan;
    }
    /// <summary>
    /// vrátí čas odjezdu autobusu
    /// </summary>
    public DateTime LeaveTime() {
        return BusInfo_.LeaveTime;
    }
    /// <summary>
    /// vrátí počet sedadel v autobusu
    /// </summary>
    public uint Seats() {
        return Seats_;
    }
    /// <summary>
    /// Volná sedadla.
    /// </summary>
    /// Naplní seznam čísly volných sedadel.
    /// <param name="Seats">seznam, do kterého se vloí čísla volných sedadel.</param>
    public void FreeSeats(out Queue<uint> Seats) {
        Seats = new Queue<uint>();
        Lock_.EnterReadLock();
        for(uint i = 0; i < Seats_; i++) {
            if(!SeatList_[(int)i].Reserved()) {
                Seats.Enqueue(i + 1);
            }
        }
        Lock_.ExitReadLock();
    }
    /// <summary>
    /// Info o autobusu.
    /// </summary>
    /// Naplní strukturu @ref BusInfo daty o autobusu.
    /// <param name="Data">struktura @ref BusInfo s daty o autobusu.</param>
    public void GetInfo(out BusInfo Data) {
        Data = BusInfo_;
    }
    /// <summary>
    /// Povolit rezervace pro autobus.
    /// </summary>
    public void Enable() {
        Lock_.EnterWriteLock();
        Enabled_ = true;
        Lock_.ExitWriteLock();
    }
    /// <summary>
    /// Zakázat rezervace pro autobus.
    /// </summary>
    public void Disable() {
        Lock_.EnterWriteLock();
        Enabled_ = false;
        Lock_.ExitWriteLock();
    }
    /// <summary>
    /// Dotaz zda je autobus povolen.
    /// </summary>
    /// <returns>@e true v případě, že je autobus povolen, @e false v případě, že je zakázán</returns>
    public bool Enabled() {
        bool result;
        Lock_.EnterReadLock();
        result = Enabled_;
        Lock_.ExitReadLock();
        return result;
    }
    /// <summary>
    /// Dočasná rezervace.
    /// </summary>
    /// Dočasná rezervace sedadel ze seznamu. Sedadla, která se povedlo rezervovat, zůstanou v seznamu, ostatní budou smazána.
    /// <returns>@e true v případě všech úspěšných rezervací, @e false v případě nějakého nezdaru při rezervaci.</returns>
    /// <param name="Seats">seznam sedadel k rezervaci.</param>
    /// <param name="Customer">ID rezervujícího.</param>
    public bool AskSeats(ref Queue<uint> Seats, uint Customer) {
        bool result = true;
        var out_queue = new Queue<uint>();
        var array = Seats.ToArray();
        Array.Sort(array);
        if(array.Length > 0) {
            Lock_.EnterWriteLock();
            if(Enabled_) {
                uint last = 0;
                for(uint i = 0; i < array.Length; i++) {
                    if(i != 0 && array[i] == last) {
                        continue;
                    }
                    if(array[i] == 0) {
                        result = false;
                    }
                    else if(array[i] > Seats_) {
                        result = false;
                    }
                    else {
                        var seat = SeatList_[(int)array[i] - 1];
                        if(seat.ReserveFor(Customer, Constants.ASK_TIMEOUT)) {
                            out_queue.Enqueue(array[i]);
                        }
                        else {
                            result = false;
                        }
                        last = array[i];
                    }
                }
            }
            else {
                result = false;
            }
            Lock_.ExitWriteLock();
        }
        Seats = out_queue;
        return result;
    }
    /// <summary>
    /// Trvalá rezervace.
    /// </summary>
    /// Trvalá rezervace sedadel ze seznamu. Sedadla, která se povedlo rezervovat, zůstanou v seznamu, ostatní budou smazána.
    /// <returns>@e true v případě všech úspěšných rezervací, @e false v případě nějakého nezdaru při rezervaci.</returns>
    /// <param name="Seats">seznam sedadel k rezervaci.</param>
    /// <param name="Customer">ID rezervujícího.</param>
    public bool LockSeats(ref Queue<uint> Seats, uint Customer) {
        bool result = true;
        var out_queue = new Queue<uint>();
        var array = Seats.ToArray();
        Array.Sort(array);
        if(array.Length > 0) {
            Lock_.EnterWriteLock();
            if(Enabled_) {
                uint last = 0;
                for(uint i = 0; i < array.Length; i++) {
                    if(i != 0 && array[i] == last) {
                        continue;
                    }
                    if(array[i] == 0) {
                        result = false;
                    }
                    else if(array[i] > Seats_) {
                        result = false;
                    }
                    else {
                        var seat = SeatList_[(int)array[i] - 1];
                        if(seat.Reserve(Customer)) {
                            out_queue.Enqueue(array[i]);
                        }
                        else {
                            result = false;
                        }
                        last = array[i];
                    }
                }
            }
            else {
                result = false;
            }
            Lock_.ExitWriteLock();
        }
        Seats = out_queue;
        return result;
    }
    /// <summary>
    /// Zrušení rezervace.
    /// </summary>
    /// Zrušení rezervace sedadel ze seznamu. Sedadla, která se povedlo uvolnit, zůstanou v seznamu, ostatní budou smazána.
    /// <returns>@e true v případě všech úspěšných zrušení rezervací, @e false v případě nějakého nezdaru.</returns>
    /// <param name="Seats">seznam sedadel k uvolnění.</param>
    /// <param name="Customer">ID rezervujícího.</param>
    public bool UnlockSeats(ref Queue<uint> Seats, uint Customer) {
        bool result = true;
        var out_queue = new Queue<uint>();
        var array = Seats.ToArray();
        Array.Sort(array);
        if(array.Length > 0) {
            Lock_.EnterWriteLock();
            if(Enabled_) {
                uint last = 0;
                for(uint i = 0; i < array.Length; i++) {
                    if(i != 0 && array[i] == last) {
                        continue;
                    }
                    if(array[i] == 0) {
                        result = false;
                    }
                    else if(array[i] > Seats_) {
                        result = false;
                    }
                    else {
                        var seat = SeatList_[(int)array[i] - 1];
                        if(seat.Free(Customer)) {
                            out_queue.Enqueue(array[i]);
                        }
                        else {
                            result = false;
                        }
                        last = array[i];
                    }
                }
            }
            else {
                result = false;
            }
            Lock_.ExitWriteLock();
        }
        Seats = out_queue;
        return result;
    }
    /// <summary>
    /// Záloha.
    /// </summary>
    /// Vytvoření zálohy třídy všech datových položek do proudu (souboru, soketu, ...).
    /// <param name="stream">výstupní proud.</param>
    public void Backup(System.IO.Stream stream) {
        Backup backup = new global::Backup(stream);
        backup.Header(BkHeader.DatObject.Bus);
        backup.Unsigned(BusInfo_.Id);
        backup.Unsigned(BusInfo_.Line);
        backup.Unsigned(BusInfo_.CityFrom);
        backup.Unsigned(BusInfo_.CityTo);
        backup.Unsigned(BusInfo_.Plan);
        backup.Time(BusInfo_.LeaveTime);
        backup.Bool(Enabled_);
        backup.Unsigned(Seats_);
        foreach(var seat in SeatList_) {
            seat.Backup(stream);
        }
    }
    private bool Enabled_;
    private uint Seats_;
    private BusInfo BusInfo_;
    private List<Seat> SeatList_;
    private ReaderWriterLockSlim Lock_;
};



/// <summary>
/// Kontajner autobusů.
/// </summary>
/// Třída sdružuje všechny autobusy. Stará se o správné přidávání, mazání a hledání autobusů,
/// díky @ref AutoIncrement i o jejich jedinečné číslování. Autobusy jsou uloženy v asociativním
/// kontajneru, řazení je prováděno dle unikátního čísla.
class Buses {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// Vytvoření prázdného kontejneru.
    public Buses() {
        BusSet_ = new TreeDictionary<uint, Bus>();
        Increment_ = new AutoIncrement();
    }
    /// <summary>
    /// Načtení z proudu.
    /// </summary>
    /// Kontajner je vytvořen načtením všech položek z proudu.
    /// <param name="stream">vstupní proud s binárními daty.</param>
    public Buses(System.IO.Stream stream) : this() {
        Restore restore = new Restore(stream);
        restore.Header(BkHeader.DatObject.Buses);
        Increment_.Set(restore.Unsigned());
        uint items = restore.Unsigned();
        for(uint i = 0; i < items; i++) {
            var temp = new Bus(stream);
            BusSet_.Add(temp.Id(), temp);
        }
    }
    /// <summary>
    /// Přidání nového autobusu.
    /// </summary>
    /// Získání unikátního ID pro autobus pomocí @ref AutoIncrement a vložení do kontejneru.
    /// <param name="Info">data potřebná k vytvoření autobusu.</param>
    /// <param name="Seats">počet sedadel autobusu.</param>
    /// <returns>nově vložený autobus</returns>
    public Bus Add(BusInfo Info, uint Seats) {
        Bus bus = new Bus(Info, Increment_.Next(), Seats);
        BusSet_.Add(bus.Id(), bus);
        return bus;
    }
    /// <summary>
    /// Smazání autobusu.
    /// </summary>
    /// <returns>@e true při úspěchu, @e false jinak</returns>
    /// <param name="Id">unikátní ID autobusu určeného ke smazání.</param>
    public bool Delete(uint Id) {
        return BusSet_.Remove(Id);
    }
    /// <summary>
    /// Nalezení autobusu podle ID.
    /// </summary>
    /// <returns>nalezený autobus nebo @e null.</returns>
    /// <param name="Id">ID hledaného autobusu.</param>
    public Bus Find(uint Id) {
        Bus bus;
        if(BusSet_.Find(ref Id, out bus)) {
            return bus;
        }
        else {
            return null;
        }
    }
    /// <summary>
    /// Nalezení dalšího autobusu v pořadí.
    /// </summary>
    /// Nalezne a vrátí ID dalšího autobusu v pořadí po @e Id. Pokud takový neexistuje, vrátí @e 0.
    public uint GetNextId(uint Id) {
        C5.KeyValuePair<uint, Bus> temp;
        if(BusSet_.TrySuccessor(Id, out temp)) {
            return temp.Key;
        }
        else {
            return 0;
        }
    }
    /// <summary>
    /// Zjištění počtu.
    /// </summary>
    /// Zjištění počtu objektů v kontejneru.
    /// <returns>aktuální počet</returns>
    public uint Count() {
        return (uint)BusSet_.Count;
    }
    /// <summary>
    /// Vymazání kontajneru.
    /// </summary>
    /// Vymazání všech položek kontajneru.
    /// @note Vymazány jsou pouze prvky kontakjneru, pokud k němu existuje nějaký index, je nutné jeho vymazání externími prostředky.
    public void Clear() {
        BusSet_.Clear();
    }
    /// <summary>
    /// Záloha.
    /// </summary>
    /// Vytvoření zálohy kontajneru včetně dalších datových položek do proudu (souboru, soketu, ...).
    /// <param name="stream">výstupní proud.</param>
    public void Backup(System.IO.Stream stream) {
        Backup backup = new global::Backup(stream);
        backup.Header(BkHeader.DatObject.Buses);
        backup.Unsigned(Increment_.Next());
        backup.Unsigned((uint)BusSet_.Count);
        foreach(var bus in BusSet_) {
            bus.Value.Backup(stream);
        }
    }
    private AutoIncrement Increment_;
    private TreeDictionary<uint, Bus> BusSet_;
};