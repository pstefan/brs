using System;
using System.Collections.Generic;
using System.Threading;
using C5;

/// <summary>
/// Město.
/// </summary>
/// Třída si kromě konstantních položek (@ref CityInfo) uchovává i počet autobusů, ve kterých vystupuje jako 
/// počáteční a cílová stanice.
class City {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="Info">struktura @ref CityInfo s daty k vytvoření města.</param>
    public City(CityInfo Info) {
        CityInfo_ = Info;
        Lock_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        LinesIn_ = 0;
        LinesOut_ = 0;
    }
    /// <summary>
    /// Načtení z proudu.
    /// </summary>
    /// Třída je vytvořena načtením všech položek z proudu.
    /// <param name="stream">vstupní proud.</param>
    public City(System.IO.Stream stream) {
        Lock_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        CityInfo_ = new CityInfo();
        Restore restore = new Restore(stream);
        restore.Header(BkHeader.DatObject.City);
        CityInfo_.Id = restore.Unsigned();
        CityInfo_.Name = restore.String();
        LinesOut_ = restore.Unsigned();
        LinesIn_ = restore.Unsigned();
    }
    /// <summary>
    /// Získání informací o městě.
    /// </summary>
    /// <param name="Info">struktura @ref CityInfo, která bude naplněna daty o městu.</param>
    public void GetInfo(out CityInfo Info) {
        Info = CityInfo_;
    }
    /// <summary>
    /// Vrátí ID města.
    /// </summary>
    public uint Id() {
        return CityInfo_.Id;
    }
    /// <summary>
    /// Vrátí řetězec s názvem města.
    /// </summary>
    public string Name() {
        return CityInfo_.Name;
    }
    /// <summary>
    /// Vrátí počet odjíždějících autobusů.
    /// </summary>
    public uint LinesOut() {
        uint result;
        Lock_.EnterReadLock();
        result = LinesOut_;
        Lock_.ExitReadLock();
        return result;
    }
    /// <summary>
    /// Změní počet odjíždějících autobusů.
    /// </summary>
    /// @note Pro odečtění autobusu se použije záporná hodnota.
    /// <param name="Change">hodnota o kterou se má změnit počítadlo odjíždějících autobusů.</param>
    public void LinesOut(int Change) {
        Lock_.EnterWriteLock();
        if(Change > 0) {
            LinesOut_ += (uint)Change;
        }
        else {
            LinesOut_ -= (uint)(-Change);
        }
        Lock_.ExitWriteLock();
    }
    /// <summary>
    /// Vrátí počet přijíždějících autobusů.
    /// </summary>
    public uint LinesIn() {
        uint result;
        Lock_.EnterReadLock();
        result = LinesIn_;
        Lock_.ExitReadLock();
        return result;
    }
    /// <summary>
    /// Změní počet přijíždějících autobusů.
    /// </summary>
    /// @note Pro odečtění autobusu se použije záporná hodnota.
    /// <param name="Change">hodnota o kterou se má změnit počítadlo přijíždějících autobusů.</param>
    public void LinesIn(int Change) {
        Lock_.EnterWriteLock();
        if(Change > 0) {
            LinesIn_ += (uint)Change;
        }
        else {
            LinesIn_ -= (uint)(-Change);
        }
        Lock_.ExitWriteLock();
    }
    /// <summary>
    /// Záloha.
    /// </summary>
    /// Vytvoření zálohy třídy všech datových položek do proudu (souboru, soketu, ...).
    /// <param name="stream">výstupní proud.</param>
    public void Backup(System.IO.Stream stream) {
        Backup backup = new global::Backup(stream);
        backup.Header(BkHeader.DatObject.City);
        backup.Unsigned(CityInfo_.Id);
        backup.String(CityInfo_.Name);
        backup.Unsigned(LinesOut_);
        backup.Unsigned(LinesIn_);
    }
    private uint LinesOut_;
    private uint LinesIn_;
    private CityInfo CityInfo_;
    private ReaderWriterLockSlim Lock_;
};


//! 
/*!
*/
/// <summary>
/// Kontajner měst.
/// </summary>
/// Třída sdružuje všechna města a díky @ref AutoIncrement zajišťuje přiřazování jedinečných čísel.
/// Města jsou uložena v asociativním kontajneru, kde jsou řazena podle unikátního čísla.
class Cities {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// Vytvoření prázdného kontejneru.
    public Cities() {
        CitySet_ = new TreeDictionary<uint, City>();
        Increment_ = new AutoIncrement();
    }
    /// <summary>
    /// Načtení z proudu.
    /// </summary>
    /// Kontajner je vytvořen načtením všech položek z proudu.
    /// <param name="stream">vstupní proud s binárními daty.</param>
    public Cities(System.IO.Stream stream) : this() {
        Restore restore = new Restore(stream);
        restore.Header(BkHeader.DatObject.Cities);
        Increment_.Set(restore.Unsigned());
        uint items = restore.Unsigned();
        for(uint i = 0; i < items; i++) {
            var temp = new City(stream);
            CitySet_.Add(temp.Id(), temp);
        }
    }
    /// <summary>
    /// Přidání města.
    /// </summary>
    /// Přidá nové město podle názvu a přiřadí mu své číslo ID.
    /// <param name="Name">název města.</param>
    /// <returns>nově přidané město</returns>
    public City Add(string Name) {
        CityInfo info = new CityInfo();

        info.Id = Increment_.Next();
        info.Name = Name;
        City city = new City(info);
        CitySet_.Add(city.Id(), city);
        return city;
    }
    /// <summary>
    /// Smazání města.
    /// </summary>
    /// Smaže město podle jeho ID.
    /// <param name="Id">ID města určeného ke smazání.</param>
    /// <returns>@e true při úspěchu, @e false při selhání</returns>
    public bool Delete(uint Id) {
        return CitySet_.Remove(Id);
    }
    /// <summary>
    /// Nalezení města.
    /// </summary>
    /// Najde město podle jeho ID a vrátí na něj ukazatel.
    /// <param name="Id">ID hledaného města.</param>
    /// <returns>nalezené město nebo @e null</returns>
    public City Find(uint Id) {
        City city;
        if(CitySet_.Find(ref Id, out city)) {
            return city;
        }
        else {
            return null;
        }
    }
    /// <summary>
    /// Nalezení dalšího města v pořadí.
    /// </summary>
    /// Nalezne a vrátí ID dalšího města v pořadí po @e Id. Pokud takové neexistuje, vrátí @e 0.
    public uint GetNextId(uint Id) {
        C5.KeyValuePair<uint, City> temp;
        if(CitySet_.TrySuccessor(Id, out temp)) {
            return temp.Key;
        }
        else {
            return 0;
        }
    }
    /// <summary>
    /// Zjištění počtu.
    /// </summary>
    /// Zjištění počtu objektů v kontajneru.
    /// <returns>aktuální počet</returns>
    public uint Count() {
        return (uint)CitySet_.Count;
    }
    /// <summary>
    /// Vymazání kontajneru.
    /// </summary>
    /// Vymazání všech položek v kontajneru.
    /// @note Vymazány jsou pouze prvky kontajneru, pokud k němu existuje nějaký index, je nutné
    /// jeho vymazání externími prostředky.
    public void Clear() {
        CitySet_.Clear();
    }
    /// <summary>
    /// Záloha.
    /// </summary>
    /// Vytvoření zálohy kontajneru včetně dalších datových položek do proudu (souboru, soketu, ...).
    /// <param name="stream">výstupní proud.</param>
    public void Backup(System.IO.Stream stream) {
        Backup backup = new global::Backup(stream);
        backup.Header(BkHeader.DatObject.Cities);
        backup.Unsigned(Increment_.Next());
        backup.Unsigned((uint)CitySet_.Count);
        foreach(var city in CitySet_) {
            city.Value.Backup(stream);
        }
    }
    private TreeDictionary<uint, City> CitySet_;
    private AutoIncrement Increment_;
};