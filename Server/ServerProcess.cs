﻿using System;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;

/// <summary>
/// Serverová třída zajišťující zpřístupnění databáze vzálenému klientovi.
/// </summary>
/// Při aktivování serveru (vyvoláním @c Start) se vytvoří pracovní vlákno, které nastaví
/// vnitřní soket do naslouchacího stavu a čeká na příchozí spojení. Jakmile se tak stane,
/// vytvoří zpracovatelské vlákno, které si pro toto spojení vytvoří klientský soket, na kterém
/// dále komunikuje, zajišťuje správné volání uživatelské funkce a nakonec spojení uzavře.
/// Pracovní vlákno serveru mezitím může čekat na další příchozí spojení, pro něj vytvořit další
/// zpracovatelské vlákno, atd. Zpracovatelských vláken může pracovat řada paralelně, takže 
/// případné delší zpracovávání příkazů neblokuje činnost dalších klientů.
/// @image html ServerTcp.png "Vlákna při činnosti TCP serveru"
/// Na diagramu je volající vlákno zobrazeno žlutými bloky, pracovní vlákno serveru pak modrými
/// bloky a jeden reprezentant zpracovatelského vlákna barvou oranžovou.
class ServerProcess {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="database">Ukazatel na databázi přes rozhraní IQuery.</param>
    /// <param name="port">Port, na kterém bude server naslouchat.</param>
    /// @note Server poslouchá na všech svých dostupných IP adresách
    public ServerProcess(IQuery database, int port) {
        database_ = database;
        port_ = port;
        listen_thread_ = null;
        server_ = null;
        thread_count_ = null;
    }
    /// <summary>
    /// Spuštění komunikační smyčky a obsluhy příchozích požadavků.
    /// </summary>
    public void Start() {
        thread_count_ = new CountdownEvent(1);
        listen_thread_ = new Thread(Run);
        listen_thread_.Start();
    }
    /// <summary>
    /// Zastavení komunikační smyčky a přijímání nových požadavků od klientů. Rozpracované požadavky před ukončením komunikace vyřízeny.
    /// </summary>
    public void Stop() {
        thread_count_.Signal(); // zrusi pocatecni jednicku
        thread_count_.Wait(); // pocka na rozpracovana vlakna
        thread_count_.Dispose();
        thread_count_ = null;
        server_.Stop();
        listen_thread_.Join();
        listen_thread_ = null;
    }
    private Thread listen_thread_;
    private TcpListener server_;
    private CountdownEvent thread_count_;
    private void Run() {
        server_ = new TcpListener(System.Net.IPAddress.Any, port_);
        server_.Start();
        while(true) {
            TcpClient newclient;
            try {
                newclient = server_.AcceptTcpClient();
            }
            catch(SocketException) {
                break;
            }
            newclient.NoDelay = true;
            thread_count_.AddCount();
            Thread t = new Thread(new ParameterizedThreadStart(ProcessClient));
            t.Start(newclient);
        }
    }
    private void ProcessClient(object client) {
        ServerConnection connection = new ServerConnection((TcpClient)client);
        QueryCmd command;
        try {
            connection.Read(out command);
            switch(command) {
            case QueryCmd.CMDADDBUS:
                AddBus(connection);
                break;
            case QueryCmd.CMDADDBUSPLAN:
                AddBusPlan(connection);
                break;
            case QueryCmd.CMDADDCITY:
                AddCity(connection);
                break;
            case QueryCmd.CMDADDCITYALIAS:
                AddCityAlias(connection);
                break;
            case QueryCmd.CMDASKSEATS:
                AskSeats(connection);
                break;
            case QueryCmd.CMDBUSINFO:
                BusInfo(connection);
                break;
            case QueryCmd.CMDBUSPLANINFO:
                BusPlanInfo(connection);
                break;
            case QueryCmd.CMDCITYINFO:
                CityInfo(connection);
                break;
            case QueryCmd.CMDDELETEBUS:
                DeleteBus(connection);
                break;
            case QueryCmd.CMDDELETEBUSPLAN:
                DeleteBusPlan(connection);
                break;
            case QueryCmd.CMDDELETECITY:
                DeleteCity(connection);
                break;
            case QueryCmd.CMDDISABLEBUS:
                DisableBus(connection);
                break;
            case QueryCmd.CMDENABLEBUS:
                EnableBus(connection);
                break;
            case QueryCmd.CMDFINDBUS:
                FindBus(connection);
                break;
            case QueryCmd.CMDFINDBUSPLAN:
                FindBusPlan(connection);
                break;
            case QueryCmd.CMDFINDCITY:
                FindCity(connection);
                break;
            case QueryCmd.CMDLISTALIAS:
                ListAlias(connection);
                break;
            case QueryCmd.CMDLISTCITY:
                ListCity(connection);
                break;
            case QueryCmd.CMDLOCKSEATS:
                LockSeats(connection);
                break;
            case QueryCmd.CMDNEWCUSTOMER:
                uint result = database_.NewCustomer();
                connection.Write(result);
                break;
            case QueryCmd.CMDNEXTBUS:
                NextBus(connection);
                break;
            case QueryCmd.CMDNEXTCITY:
                NextCity(connection);
                break;
            case QueryCmd.CMDNEXTPLAN:
                NextPlan(connection);
                break;
            case QueryCmd.CMDUNLOCKSEATS:
                UnlockSeats(connection);
                break;
            default:
                break;
            }
        }
        catch(System.IO.IOException) {
            return;
        }
        finally {
            thread_count_.Signal();
            connection.Close();
        }
    }
    private void AddBusPlan(ServerConnection con) {
        string name;
        uint seats;
        string data;
        con.Read(out name);
        con.Read(out seats);
        con.Read(out data);
        con.Write(database_.AddBusPlan(name, seats, data));
    }
    private void AddBus(ServerConnection con) {
        BusInfo info = new BusInfo();
        con.Read(out info.Id);
        con.Read(out info.Line);
        con.Read(out info.CityFrom);
        con.Read(out info.CityTo);
        con.Read(out info.LeaveTime);
        con.Read(out info.Plan);
        con.Write(database_.AddBus(info));
    }
    private void BusInfo(ServerConnection con) {
        uint bus;
        con.Read(out bus);
        BusInfo info;
        bool enabled;
        uint seats;
        Queue<uint> free_seats;
        bool result = database_.BusInfo(bus, out info, out enabled, out seats, out free_seats);
        con.Write(result);
        if(result) {
            con.Write(info.Id);
            con.Write(info.Line);
            con.Write(info.CityFrom);
            con.Write(info.CityTo);
            con.Write(info.LeaveTime);
            con.Write(info.Plan);
            con.Write(enabled);
            con.Write(seats);
            uint count = (uint)free_seats.Count;
            con.Write(count);
            for(uint i = 0; i < count; i++) {
                con.Write(free_seats.Dequeue());
            }
        }
    }
    private void FindBus(ServerConnection con) {
        uint from;
        uint to;
        DateTime time;
        con.Read(out from);
        con.Read(out to);
        con.Read(out time);
        con.Write(database_.FindBus(from, to, time));
    }
    private void AddCityAlias(ServerConnection con) {
        string name;
        uint id;
        con.Read(out name);
        con.Read(out id);
        con.Write(database_.AddCityAlias(name, id));
    }
    private void BusPlanInfo(ServerConnection con) {
        uint plan;
        con.Read(out plan);
        PlanInfo info;
        uint buses;
        string data;
        bool result = database_.BusPlanInfo(plan, out info, out buses, out data);
        con.Write(result);
        if(result) {
            con.Write(info.Id);
            con.Write(info.Name);
            con.Write(info.Seats);
            con.Write(buses);
            con.Write(data);
        }
    }
    private void FindCity(ServerConnection con) {
        string name;
        con.Read(out name);
        con.Write(database_.FindCity(name));
    }
    private void AddCity(ServerConnection con) {
        string name;
        con.Read(out name);
        con.Write(database_.AddCity(name));
    }
    private void DeleteCity(ServerConnection con) {
        string name;
        con.Read(out name);
        con.Write(database_.DeleteCity(name));
    }
    private void DeleteBusPlan(ServerConnection con) {
        string name;
        con.Read(out name);
        con.Write(database_.DeleteBusPlan(name));
    }
    private void FindBusPlan(ServerConnection con) {
        string name;
        con.Read(out name);
        con.Write(database_.FindBusPlan(name));
    }
    private void DeleteBus(ServerConnection con) {
        uint id;
        con.Read(out id);
        con.Write(database_.DeleteBus(id));
    }
    private void CityInfo(ServerConnection con) {
        uint city;
        con.Read(out city);
        CityInfo info;
        uint lines_out;
        uint lines_in;
        bool result = database_.CityInfo(city, out info, out lines_out, out lines_in);
        con.Write(result);
        if(result) {
            con.Write(info.Id);
            con.Write(info.Name);
            con.Write(lines_out);
            con.Write(lines_in);
        }
    }
    private void DisableBus(ServerConnection con) {
        uint bus;
        con.Read(out bus);
        con.Write(database_.DisableBus(bus));
    }
    private void EnableBus(ServerConnection con) {
        uint bus;
        con.Read(out bus);
        con.Write(database_.EnableBus(bus));
    }
    private void ListCity(ServerConnection con) {
        string From;
        con.Read(out From);
        string To;
        bool result = database_.ListCity(From, out To);
        con.Write(result);
        if(result) {
            con.Write(To);
        }
    }
    private void ListAlias(ServerConnection con) {
        string From;
        con.Read(out From);
        string Alias;
        string City;
        bool result = database_.ListAlias(From, out Alias, out City);
        con.Write(result);
        if(result) {
            con.Write(Alias);
            con.Write(City);
        }
    }
    private void NextBus(ServerConnection con) {
        uint bus;
        con.Read(out bus);
        con.Write(database_.NextBus(bus));
    }
    private void NextPlan(ServerConnection con) {
        uint plan;
        con.Read(out plan);
        con.Write(database_.NextPlan(plan));
    }
    private void NextCity(ServerConnection con) {
        uint city;
        con.Read(out city);
        con.Write(database_.NextCity(city));
    }
    private void AskSeats(ServerConnection con) {
        uint bus;
        uint customer;
        Queue<uint> free_seats = new Queue<uint>();
        con.Read(out bus);
        con.Read(out customer);
        uint count;
        con.Read(out count);
        for(uint i = 0; i < count; i++) {
            uint item;
            con.Read(out item);
            free_seats.Enqueue(item);
        }
        bool result = database_.AskSeats(bus, ref free_seats, customer);
        con.Write(result);
        count = (uint)free_seats.Count;
        con.Write(count);
        for(uint i = 0; i < count; i++) {
                con.Write(free_seats.Dequeue());
        }
    }
    private void LockSeats(ServerConnection con) {
        uint bus;
        uint customer;
        Queue<uint> free_seats = new Queue<uint>();
        con.Read(out bus);
        con.Read(out customer);
        uint count;
        con.Read(out count);
        for(uint i = 0; i < count; i++) {
            uint item;
            con.Read(out item);
            free_seats.Enqueue(item);
        }
        bool result = database_.LockSeats(bus, ref free_seats, customer);
        con.Write(result);
        count = (uint)free_seats.Count;
        con.Write(count);
        for(uint i = 0; i < count; i++) {
            con.Write(free_seats.Dequeue());
        }
    }
    private void UnlockSeats(ServerConnection con) {
        uint bus;
        uint customer;
        Queue<uint> free_seats = new Queue<uint>();
        con.Read(out bus);
        con.Read(out customer);
        uint count;
        con.Read(out count);
        for(uint i = 0; i < count; i++) {
            uint item;
            con.Read(out item);
            free_seats.Enqueue(item);
        }
        bool result = database_.UnlockSeats(bus, ref free_seats, customer);
        con.Write(result);
        count = (uint)free_seats.Count;
        con.Write(count);
        for(uint i = 0; i < count; i++) {
            con.Write(free_seats.Dequeue());
        }
    }
    private IQuery database_;
    private int port_;
}
