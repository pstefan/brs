﻿using System;

/// <summary>
/// Třída obsahuje některé konstanty používané v programu.
/// </summary>
class Constants {
    /// <summary>
    /// Délka předběžné rezervace sedadla.
    /// </summary>
    public const uint ASK_TIMEOUT = 100;
    /// <summary>
    /// Maximální velikost žurnálu před provedením zálohy.
    /// </summary>
    public const uint JOURNAL_LIMIT = 20;
    /// <summary>
    /// Hlavní hlavička žurnálu v žurnálovacím souboru.
    /// </summary>
    public const uint JournalHeader = 0x6c6e724a;
}
