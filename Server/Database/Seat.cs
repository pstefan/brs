using System;


/// <summary>
/// Sedadlo v autobusu.
/// </summary>
/// Třída slouží jako rozhraní jednotlivých sedadel. Zajišťuje správné rezervování a uvolňování sedadel. 
/// Rezervaci sedadla je možné provést pouze dočasně (na omezený časový úsek) což umožní zákazníkovi 
/// potvrzení požadavku bez nebezpečí, že požadovaná sedadla mezitím rezervuje někdo jiný. Poté může 
/// určený zákazník rezervaci potvrdit (tj. nastavit ji jako časově neomezenou) nebo odmítnout. Pokud 
/// se tak nestane během určené "ochranné" doby, je sedadlo opět pokládáno za volné.
class Seat {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// Sedadlo je vytvořené jako nerezervované.
    public Seat() {
        Expiry_ = DateTime.MinValue;
    }
    /// <summary>
    /// Načtení z proudu.
    /// </summary>
    /// Třída je vytvořena načtením všech položek z proudu.
    /// <param name="stream">vstupní proud.</param>
    public Seat(System.IO.Stream stream) {
        Restore restore = new Restore(stream);
        restore.Header(BkHeader.DatObject.Seat);
        Reserved_ = restore.Bool();
        Expiry_ = restore.Time();
        Customer_ = restore.Unsigned();
    }
    /// <summary>
    /// Konečná rezervace.
    /// </summary>
    /// Zarezervování sedadla napořád, tj. do doby odjezdu autobusu.
    /// <param name="Customer">jedinečné ID zákazníka, nutné pro případnou deaktivaci rezervace.</param>
    /// <returns>@e true pri úspěchu.</returns>
    public bool Reserve(uint Customer) {
        if(!Reserved(DateTime.Now) || (Customer == Customer_ && Expiry_ != DateTime.MinValue)) {
            // nebylo rezervovano nebo bylo docasne stavajicim zakaznikem
            Reserved_ = true;
            Expiry_ = DateTime.MinValue;
            Customer_ = Customer;
            return true;
        }
        else {
            return false;
        }
    }
    /// <summary>
    /// Časově omezená rezervace.
    /// </summary>
    /// Rezervace sedadla, která po určitém čase vyprší.
    /// <returns>@e true při úspěchu.</returns>
    /// <param name="Customer">jedinečné ID zákazníka, nutné pro případnou deaktivaci rezervace.</param>
    /// <param name="Seconds">délka trvání rezervace ve vteřinách.</param>
    public bool ReserveFor(uint Customer, uint Seconds) {
        DateTime now = DateTime.Now;
        if(!Reserved(now)) {
            Reserved_ = true;
            Expiry_ = now.AddSeconds((double)Seconds);
            Customer_ = Customer;
            return true;
        }
        else {
            return false;
        }
    }
    /// <summary>
    /// Zrušení rezervace.
    /// </summary>
    /// Zrušení rezervace sedadla.
    /// <param name="Customer">jedinečné ID zákazníka které musí být shodné s ID uvedeným při rezervaci.</param>
    /// <returns>@e true při úspěchu</returns>
    public bool Free(uint Customer) {
        bool reserved = Reserved(DateTime.Now);
        if(!reserved || Customer_ != Customer) {
            return false;
        }
        else {
            Reserved_ = false;
            return true;
        }
    }
    /// <summary>
    /// Dotaz zda je sedadlo rezervováno.
    /// </summary>
    /// <returns>@e true v případě platné rezervace, @e false v případě volného sedadla</returns>
    public bool Reserved() {
        return Reserved(DateTime.Now);
    }
    /// <summary>
    /// Záloha.
    /// </summary>
    /// Vytvoření zálohy třídy všech datových položek do proudu (souboru, soketu, ...).
    /// <param name="stream">výstupní proud.</param>
    public void Backup(System.IO.Stream stream) {
        Backup backup = new global::Backup(stream);
        backup.Header(BkHeader.DatObject.Seat);
        backup.Bool(Reserved_);
        backup.Time(Expiry_);
        backup.Unsigned(Customer_);
    }
    private bool Reserved(DateTime Now) {
        // reservovano trvale nebo jiz vyprsela docasna rezervace
        return Reserved_ && (Expiry_ == DateTime.MinValue || Now <= Expiry_);
    }
    private bool Reserved_;
    private DateTime Expiry_;
    private uint Customer_;
};
