﻿using System;
using System.Collections.Generic;

/// <summary>
/// typ metody pro zpracování příkazu.
/// </summary>
delegate void FnCommand();

/// <summary>
/// Hlavní třída klientské aplikace.
/// </summary>
/// Tato třída zastřešuje většinu funkce klientské aplikace.
class ClientApp {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="Query">Databáze prostřednictvím rozhraní IQuery.</param>
    /// <param name="IsFull">@e true pokud je tento klient plnohodnotný, @e false pokud má jen omezené množství příkazů.</param>
    public ClientApp(IQuery Query, bool IsFull) {
        Logic_ = new Logic(Query);
        Commands_ = new Dictionary<string, FnCommand>();
        Graphics_ = new Graphics();
        Messages_ = new Dictionary<Error, string>();
        IsFull_ = IsFull;
        Input_ = null;
        Commands_.Add("find", this.Find);
        Commands_.Add("next", Next);
        Commands_.Add("bus", Bus);
        Commands_.Add("reserve", Reserve);
        Commands_.Add("unlock", Unlock);
        Commands_.Add("city", City);
        if(IsFull) {
            Commands_.Add("addbus", AddBus);
            Commands_.Add("delbus", DelBus);
            Commands_.Add("addcity", AddCity);
            Commands_.Add("delcity", DelCity);
            Commands_.Add("addcityalias", AddCityAlias);
            Commands_.Add("addbusplan", AddBusPlan);
            Commands_.Add("delbusplan", DelBusPlan);
            Commands_.Add("clear", Clear);
            Commands_.Add("close", Close);
        }

        Messages_.Add(Error.NOERR, "OK");
        Messages_.Add(Error.CITYFROM, "Write city from.");
        Messages_.Add(Error.CITYTO, "Write city to.");
        Messages_.Add(Error.LEAVETIME, "Write leave time.");
        Messages_.Add(Error.CTFROMNFOUND, "City from not found.");
        Messages_.Add(Error.CTTONFOUND, "City to not found.");
        Messages_.Add(Error.CTNFOUND, "City not found.");
        Messages_.Add(Error.TIMEFORMAT, "Wrong date/time format.");
        Messages_.Add(Error.BUSNFOUND, "Bus not found.");
        Messages_.Add(Error.NBUSESFOUND, "No buses found.");
        Messages_.Add(Error.NUMSEATS, "Write number of seats.");
        Messages_.Add(Error.NBUSSET, "No bus set. Use BUS command.");
        Messages_.Add(Error.NUSERSET, "No customer number set. Use NEW command.");
        Messages_.Add(Error.NFREESEATS, "Not so many free seats.");
        Messages_.Add(Error.WRITESEATS, "Write list of seats to reserve.");
        Messages_.Add(Error.NRESERVE, "Cannot reserve required seats.");
        Messages_.Add(Error.NUNLOCK, "Cannot unlock all required seats.");
        Messages_.Add(Error.WRITEBLINE, "Write bus line.");
        Messages_.Add(Error.WRITEBPLAN, "Write bus plan.");
        Messages_.Add(Error.PLANNFOUND, "Bus plan not found.");
        Messages_.Add(Error.WRITEBID, "Write bus number.");
        Messages_.Add(Error.NDELBUS, "Cannot delete bus.");
        Messages_.Add(Error.WRCITY, "Write city name.");
        Messages_.Add(Error.TOOMANYARGS, "Too many arguments. Unused ones are ignored.");
        Messages_.Add(Error.NADDCITY, "Cannot add city.");
        Messages_.Add(Error.NDELCITY, "Cannot delete city.");
        Messages_.Add(Error.NADDALIAS, "Cannot add city alias.");
        Messages_.Add(Error.WRALIAS, "Write new alias and existing city name.");
        Messages_.Add(Error.WRBUSPLAN, "Write name of bus plan.");
        Messages_.Add(Error.WRSEATSCOUNT, "Write number of seats.");
        Messages_.Add(Error.NADDPLAN, "Cannot add bus plan.");
        Messages_.Add(Error.NDELPLAN, "Cannot delete bus plan.");
        Messages_.Add(Error.NFILE, "Write proper bus plan file.");
        Messages_.Add(Error.NADDBUS, "Cannot add bus.");
        Messages_.Add(Error.WRONGBID, "Wrong bus number.");
        Messages_.Add(Error.FIRSTFIND, "Use FIND first.");
        Messages_.Add(Error.WRUSERID, "Write customer number.");
        Messages_.Add(Error.NTYPESET, "Write what to clear.");
        Messages_.Add(Error.WRARGUMENT, "Wrong argument.");
        Messages_.Add(Error.FILEREADFAIL, "Cannot read the file.");
        Messages_.Add(Error.BUSDISABLED, "Reservations disabled in this bus.");
        Messages_.Add(Error.USEDBUS, "Cannot delete this bus. Wait until it leaves or disable it first.");
        Messages_.Add(Error.NDISBUS, "Cannot disable bus.");
    }
    /// <summary>
    /// Spuštění zpracovatelské smyčky.
    /// </summary>
    public bool Run() {
        Graphics_.WriteWelcome();
        try {
            Logic_.Init();
        }
        catch(System.Net.Sockets.SocketException) {
            Graphics_.Write("Connection to server failed. Please try again later.");
            return false;
        }
        catch(System.IO.IOException) {
            Graphics_.Write("Connection to server was interrupted. Please try again later.");
            return false;
        }
        do {
            // chráněný blok pro zachycení výjimek způsobených chybou komunikace se serverem
            try {
                //Logic_.Init();
                while(true) {
                    // načti řádek ze vstupu
                    while(Input_ == null || Input_ == "") {
                        if(!Graphics_.Read(out Input_)) {
                            return true; // např. při EOF ze souboru přesměrovaného na vstup
                        }
                    }
                    InpSplited_ = Input_.Split(' ');
                    ParamIndex_ = 0;
                    string Comm = GetOneParam().ToLower(); // vyzvednutí příkazu
                    if(Comm == "exit")
                        return true;
                    else if(Comm == "help")
                        Graphics_.WriteHelp(IsFull_);
                    else
                        ProcessCommand(Comm); // zpracování příkazu
                    Input_ = null;
                }
            }
            catch(System.Net.Sockets.SocketException) {
                Graphics_.Write("Connection to server failed. Please try again later.");
                Input_ = null;
            }
            catch(System.IO.IOException) {
                Graphics_.Write("Connection to server was interrupted. Please try again later.");
                Input_ = null;
            }
            catch(Exception e) { // vypsání příčiny výjimky
                Graphics_.Write("Fatal error:");
                Graphics_.Write(e.Message);
                Input_ = null;
            }
        } while(Graphics_.Continue()); // další pokus po chybě a potvrzení uživatelem
        return false;
    }
    private Logic Logic_;
    private Graphics Graphics_;
    private string Input_;
    private string[] InpSplited_;
    private uint ParamIndex_;
    private Dictionary<string, FnCommand> Commands_;
    private Dictionary<Error, string> Messages_;
    private string GetOneParam() {
        while(ParamIndex_ < InpSplited_.Length && InpSplited_[ParamIndex_] == "") {
            ParamIndex_++;
        }
        if(ParamIndex_ >= InpSplited_.Length) {
            return null;
        }
        else if(InpSplited_[ParamIndex_][0] == '"') {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.Append(InpSplited_[ParamIndex_].Substring(1));
            if(InpSplited_[ParamIndex_][InpSplited_[ParamIndex_].Length - 1] == '"') {
                ParamIndex_++;
                builder.Remove(builder.Length - 1, 1);
                return builder.ToString();
            }
            ParamIndex_++;
            while(ParamIndex_ < InpSplited_.Length && InpSplited_[ParamIndex_][InpSplited_[ParamIndex_].Length - 1] != '"') {
                builder.Append(' ');
                builder.Append(InpSplited_[ParamIndex_]);
                ParamIndex_++;
            }
            if(ParamIndex_ < InpSplited_.Length && InpSplited_[ParamIndex_][InpSplited_[ParamIndex_].Length - 1] == '"') {
                builder.Append(' ');
                builder.Append(InpSplited_[ParamIndex_].Substring(0, InpSplited_[ParamIndex_].Length - 1));
                ParamIndex_++;
            }
            return builder.ToString();
        }
        else {
            return InpSplited_[ParamIndex_++];
        }
    }
    private uint GetNumber(string Number) {
        return UInt32.Parse(Number);
    }
    private bool GetList(out Queue<uint> Seats, string Param) {
        Seats = new Queue<uint>();
        if(Param == null) {
            return true;
        }
        var splited = Param.Split(',');
        for(uint i = 0; i < splited.Length; i++) {
            if(splited[i] == "") {
                Seats.Clear();
                return false;
            }
            else {
                uint number;
                if(UInt32.TryParse(splited[i], out number)) {
                    Seats.Enqueue(number);
                }
                else {
                    Seats.Clear();
                    return false;
                }
            }
        }
        return true;
    }
    private void ProcessCommand(string Comm) {
        FnCommand function;
        if(Commands_.TryGetValue(Comm, out function)) {
            function();
        }
        else {
            Graphics_.Write("Unknown command.");
        }
    }
    private string Messages(Error Number) {
        string result;
        Messages_.TryGetValue(Number, out result);
        return result;
    }
    private bool IsFull_;
    // metody typu FnCommand
    private void Find() {
        Queue<BusPrint> QBusPrint;
        string CityFrom = GetOneParam();
        string CityTo = GetOneParam();
        string LeaveTime = GetOneParam();
        if(!Logic_.Find(out QBusPrint, CityFrom, CityTo, LeaveTime))
            Graphics_.Write(Messages(Logic_.GetLastError()));
        if(QBusPrint != null && QBusPrint.Count != 0)
            Graphics_.WriteBuses(QBusPrint);
    }
    private void Next() {
        Queue<BusPrint> busPrint;
        Logic_.Next(out busPrint);
        if(busPrint.Count != 0)
            Graphics_.WriteBuses(busPrint);
        else
            Graphics_.Write(Messages(Logic_.GetLastError()));
    }
    private void Bus() {
        string TextBusPlan;
        Queue<uint> Seats;
        BusPrint busPrint;
        uint NumberSeats;
        bool Ok = Logic_.SetBus(GetNumber(GetOneParam()), out Seats, out TextBusPlan, out busPrint, out NumberSeats);
        Graphics_.Write(Messages(Logic_.GetLastError()));
        if(Ok)
            Graphics_.WriteBusPlan(TextBusPlan, Seats, busPrint, NumberSeats);
    }
    private void Reserve() {
        string TextNumber;
        Queue<uint> Seats;
        BusPrint busPrint;
        TextNumber = GetOneParam();
        // nejprve předběžná rezervace
        if(TextNumber[0] == '#') { // rezervace počtu sedadel (výběr na programu)
            TextNumber = TextNumber.Substring(1);
            uint NumSeats = GetNumber(TextNumber);
            if(!Logic_.ReserveAsk(out Seats, NumSeats)) {
                Graphics_.Write(Messages(Logic_.GetLastError()));
                return;
            }
        }
        else { // rezervace vybraných sedadel (určeno zákazníkem)
            GetList(out Seats, TextNumber);
            if(!Logic_.ReserveAsk(ref Seats)) {
                Graphics_.Write(Messages(Logic_.GetLastError()));
                Logic_.Unlock(ref Seats);
                return;
            }
        }
        // konečná rezervace po potvrzení zákazníkem
        Graphics_.WriteSeats("Seats to be reserved: ", Seats);
        if(Graphics_.Continue()) {
            uint Customer;
            if(Logic_.ReserveForever(ref Seats, out Customer, out busPrint)) {
                Graphics_.Write(Messages(Logic_.GetLastError()));
                Graphics_.WriteReservation(Customer, Seats, busPrint);
            }
            else
                Graphics_.Write(Messages(Logic_.GetLastError()));
        }
        else { // nepotvrzeno
            if(Logic_.Unlock(ref Seats))
                Graphics_.Write("Reservation canceled.");
            else
                Graphics_.Write(Messages(Logic_.GetLastError()));
        }
    }
    private void Unlock() {
        string TextNumber = GetOneParam();
        if(TextNumber == null || TextNumber.Length == 0) {
            Graphics_.Write(Messages(Error.WRUSERID));
            return;
        }
        uint Id = GetNumber(TextNumber);
        string Temp = GetOneParam();
        Queue<uint> Seats;
        if(Temp == null || Temp.Length == 0)
            Logic_.UnlockAll(Id, out Seats);
        else {
            GetList(out Seats, Temp);
            Logic_.Unlock(Id, ref Seats);
        }
        Graphics_.Write(Messages(Logic_.GetLastError()));
        Graphics_.WriteSeats("Unlocked seats: ", Seats);
    }
    private void AddBus() {
        string Line = GetOneParam();
        string CityFrom = GetOneParam();
        string CityTo = GetOneParam();
        string LeaveTime = GetOneParam();
        string Plan = GetOneParam();
        uint Id = Logic_.AddBus(GetNumber(Line), CityFrom, CityTo, LeaveTime, Plan);
        if(Id == 0)
            Graphics_.Write(Messages(Logic_.GetLastError()));
        else
            Graphics_.WriteAddBus(Id);
    }
    private void DelBus() {
        Logic_.DelBus(GetNumber(GetOneParam()));
        Graphics_.Write(Messages(Logic_.GetLastError()));
    }
    private void AddCity() {
        uint Id = Logic_.AddCity(GetOneParam());
        if(Id == 0)
            Graphics_.Write(Messages(Logic_.GetLastError()));
        else
            Graphics_.WriteAddCity(Id);
    }
    private void DelCity() {
        Logic_.DelCity(GetOneParam());
        Graphics_.Write(Messages(Logic_.GetLastError()));
    }
    private void AddCityAlias() {
        string AliasName = GetOneParam();
        string CityName = GetOneParam();
        Logic_.AddCityAlias(AliasName, CityName);
        Graphics_.Write(Messages(Logic_.GetLastError()));
    }
    private void AddBusPlan() {
        string Plan = GetOneParam();
        string SeatsCount = GetOneParam();
        string File = GetOneParam();
        uint Id = Logic_.AddBusPlan(Plan, GetNumber(SeatsCount), File);
        if(Id == 0)
            Graphics_.Write(Messages(Logic_.GetLastError()));
        else
            Graphics_.WriteAddPlan(Id);
    }
    private void DelBusPlan() {
        Logic_.DelBusPlan(GetOneParam());
        Graphics_.Write(Messages(Logic_.GetLastError()));
    }
    private void Clear() {
        string ParamT = GetOneParam();
        if(ParamT == null || ParamT.Length == 0) {
            Graphics_.Write(Messages(Error.NTYPESET));
            return;
        }
        string Param = ParamT.ToLower();
        if(Param == "bus")
            Graphics_.WriteDeleted(Logic_.ClearBus());
        else if(Param == "city")
            Graphics_.WriteDeleted(Logic_.ClearCity());
        else if(Param == "plan")
            Graphics_.WriteDeleted(Logic_.ClearPlan());
        else
            Graphics_.Write(Messages(Error.WRARGUMENT));
    }
    private void Close() {
        string Param = GetOneParam();
        if(Param == null || Param.Length == 0) {
            Graphics_.Write(Messages(Error.WRITEBID));
            return;
        }
        Queue<uint> ReservedSeats;
        uint Seats;
        BusPrint Bus;
        string Plan;
        bool f = Logic_.Close(GetNumber(Param), out ReservedSeats, out Plan, out Bus, out Seats);
        Graphics_.Write(Messages(Logic_.GetLastError()));
        if(f) {
            Graphics_.WriteBusPlan(Plan, ReservedSeats, Bus, Seats, "Reserved seats: ");
        }
    }
    private void City() {
        string Param = GetOneParam();
        List<KeyValuePair<string, string>> Cities; //prvni je cely nazev, druhy je alias
        Logic_.City(Param, out Cities);
        if(Cities.Count != 0)
            Graphics_.WriteCities(Cities);
        else
            Graphics_.Write(Messages(Error.CTNFOUND));
    }
}


