﻿using System;

/// <summary>
/// Třída složící k přenosu informací o databázi.
/// </summary>
/// Použití má při tisku informací o databázi příkazem @c INFO.
class DbInfo {
    //! počet měst
    public uint Cities;
    //! počet aliasů měst
    public uint CityAliases;
    //! počet plánů autobusů
    public uint Plans;
    //! počet autobusů
    public uint Buses;
};

/// <summary>
/// Administrátorské rozhraní databáze.
/// </summary>
/// Rozhraní databáze, které je nutné k administrátorským úkolům. Hlavní využití je zálohování.
interface IAdmin {
    /// <summary>
    /// Vytvoří zálohu do zadaného výstupního proudu.
    /// </summary>
    /// <returns>@e true v případě úspěchu, @e false jinak</returns>
    /// <param name="stream">Výstupní proud pro zapsání dat.</param>
    bool Backup(System.IO.Stream stream);
    /// <summary>
    /// Vrátí informace o databázi.
    /// </summary>
    /// <returns>@e true v případě úspěchu, @e false jinak</returns>
    /// <param name="Info">Ukazatel na strukturu DbInfo, kam bude vrácena tato struktura vyplněná daty.</param>
    bool Info(out DbInfo Info);
}
