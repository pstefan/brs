﻿using System;
using System.Collections.Generic;

/// <summary>
/// Typ metody pro zpracování příkazu.
/// </summary>
delegate void FnCommand();

//! 
/// <summary>
/// Hlavní třída klientské aplikace.
/// </summary>
/// Tato třída zastřešuje většinu funkce serverové aplikace z uživatelského pohledu. Tedy stará se spouštění a inicializaci databáze a také
/// o serverový terminál včetně implementace logiky příkazů, uživatelkého vstupu a výstupu na terminál.
class ServerApp {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="Query">ukazatel na databázi.</param>
    /// <param name="net_process">třída obsluhující jednotlivé klienty.</param>
    public ServerApp(MemoryDb Query, ServerProcess net_process) {
        Commands_ = new Dictionary<string, FnCommand>();
        Input_ = null;
        running_ = false;
        database_ = Query;
        net_process_ = net_process;
        Commands_.Add("backup", Backup);
        Commands_.Add("info", Info);
        Commands_.Add("start", Start);
        Commands_.Add("stop", Stop);
        Commands_.Add("help", Help);
    }
    /// <summary>
    /// Spuštění zpracovatelské smyčky.
    /// </summary>
    /// <param name="info">@e true, pokud se má po startu na konzoli vypsat informace o databázi.</param>
    public bool Run(bool info) {
        Start();
        if(info)
            Info();
        while(true) {
            // načti řádek ze vstupu
            while(Input_ == null || Input_ == "") {
                if(!Read(out Input_)) {
                    return true; // např. při EOF ze souboru přesměrovaného na vstup
                }
            }
            InpSplited_ = Input_.Split(' ');
            ParamIndex_ = 0;
            string Comm = GetOneParam().ToLower(); // vyzvednutí příkazu
            if(Comm == "exit")
                break;
            else {
                FnCommand function;
                if(Commands_.TryGetValue(Comm, out function)) {
                    function();
                }
                else {
                    Console.WriteLine("Unknown command.");
                }
            }
            Input_ = null;
        }
        Stop();
        return true;

    }
    private MemoryDb database_;
    private ServerProcess net_process_;
    private string Input_;
    private string[] InpSplited_;
    private uint ParamIndex_;
    private bool running_;
    private Dictionary<string, FnCommand> Commands_;
    private bool Read(out string Buffer) {
        Console.Write("> ");
        try {
            Buffer = Console.ReadLine();
        }
        catch(System.IO.IOException) {
            Buffer = null;
            return false;
        }
        if(Buffer == null) {
            return false;
        }
        return true;
    }
    private string GetOneParam() {
        while(ParamIndex_ < InpSplited_.Length && InpSplited_[ParamIndex_] == "") {
            ParamIndex_++;
        }
        if(ParamIndex_ >= InpSplited_.Length) {
            return null;
        }
        else if(InpSplited_[ParamIndex_][0] == '"') {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.Append(InpSplited_[ParamIndex_].Substring(1));
            if(InpSplited_[ParamIndex_][InpSplited_[ParamIndex_].Length - 1] == '"') {
                ParamIndex_++;
                builder.Remove(builder.Length - 1, 1);
                return builder.ToString();
            }
            ParamIndex_++;
            while(ParamIndex_ < InpSplited_.Length && InpSplited_[ParamIndex_][InpSplited_[ParamIndex_].Length - 1] != '"') {
                builder.Append(' ');
                builder.Append(InpSplited_[ParamIndex_]);
                ParamIndex_++;
            }
            if(ParamIndex_ < InpSplited_.Length && InpSplited_[ParamIndex_][InpSplited_[ParamIndex_].Length - 1] == '"') {
                builder.Append(' ');
                builder.Append(InpSplited_[ParamIndex_].Substring(0, InpSplited_[ParamIndex_].Length - 1));
                ParamIndex_++;
            }
            return builder.ToString();
        }
        else {
            return InpSplited_[ParamIndex_++];
        }
    }
    // metody typu FnCommand
    private void Backup() {
        string path = GetOneParam();
        if(path == null || path == "") {
            Console.WriteLine("Write backup file name.");
            return;
        }
        System.IO.FileStream stream;
        try {
            stream = new System.IO.FileStream(path, System.IO.FileMode.Create);
        }
        catch(Exception) {
            Console.WriteLine("Cannot open file for writing.");
            return;
        }
        if(!database_.Backup(stream)) {
            Console.WriteLine("Backup failed.");
        }
        else {
            Console.WriteLine("Backup succeeded - written {0} B.", stream.Length);
        }
        stream.Close();
    }
    private void Info() {
        if(running_)
            Console.WriteLine("Communication server is running.");
        else
            Console.WriteLine("Communication server is stopped.");
        DbInfo Info;
        database_.Info(out Info);
        Console.WriteLine("Cities:   {0}", Info.Cities);
        Console.WriteLine("Aliases:  {0}", Info.CityAliases);
        Console.WriteLine("Plans:    {0}", Info.Plans);
        Console.WriteLine("Buses:    {0}", Info.Buses);

    }
    private void Start() {
        if(!running_) {
            database_.OnStart();
            net_process_.Start();
            running_ = true;
            Console.WriteLine("Servers started successfully.");
        }
    }
    private void Stop() {
        if(running_) {
            net_process_.Stop();
            running_ = false;
            database_.OnStop();
            Console.WriteLine("Servers stopped successfully.");
        }

   }
    private void Help() {
        Console.WriteLine("List of available commands:");
        Console.WriteLine("EXIT\tcloses the program");
        Console.WriteLine("BACKUP\tbackups database to a file\n\tExample:\tbackup database.bac");
        Console.WriteLine("START\tstarts connection servers");
        Console.WriteLine("STOP\tstops connection servers");
        Console.WriteLine("INFO\twrites some information about servers and database");
        Console.WriteLine("HELP\tprints this help page");
        Console.WriteLine("\nCommands are case insensitive.");

    }
}

