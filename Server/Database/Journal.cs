using System;
using System.Threading;
using System.Collections.Generic;

/// <summary>
/// Třída pro obnovení dat z žurnálu.
/// </summary>
/// S pomocí ústřední metody @ref Replay je možné provést obnovení dat, která se (patrně 
/// vlivem selhání hardware) nedostala do interní databáze.
class ReadJournal {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// Vytvoření instance třídy s nastavením parametrů.
    /// <param name="Path">složka, ve které se bude hledat soubor žurnálu.</param>
    /// <param name="Db">ukazatel na instanci databáze.</param>
    public ReadJournal(string Path, MemoryDb Db) {
        Path_ = Path;
        Db_ = Db;
    }
    /// <summary>
    /// Přenesení položek žurnálu do databáze.
    /// </summary>
    /// Hlavní metoda třídy slouží k přenesení všech položek ze žurnálu (tj. těch, které se dříve 
    /// nedostaly do kompletní interní zálohy databáze). Postupně jsou jednotlivé položky vyzvedávány 
    /// ze souboru, jsou vyzvednuty a správně konvertovány jednotlivé parametry a následně se volají 
    /// příslušné metody databáze pro provedení požadovaných činností. Při každém volání databázové 
    /// metody se kontroluje návratová hodnota.
    /// <param name="Items">určuje počet zpracovaných položek. Ten v případě úspěšně zpracovaného 
    /// žurnálu je roven celkovému počtu položek, v případě neúspěchu odkazuje na položku, při jejímž 
    /// zpracování došlo k chybě.</param>
    /// <returns>úspěch operace. Hodnota @e true pro úspěšné zpracování celého souboru žurnálu nebo 
    /// v případě, že soubor žurnálu zcela chybí nebo je prázdný. Hodnota @e false značí chybu - 
    /// špatný formát souboru nebo neúspěch konkrétní databázové operace.</returns>
    public bool Replay(out uint Items) {
        uint Header;
        QueryCmd Command;

        Items = 0;

        try {
            Journal_ = new System.IO.BinaryReader(new System.IO.FileStream(Path_ + JournalName_, System.IO.FileMode.Open));
        }
        catch(Exception) {
            return true; // zadny zurnal
        }
        bool Result = true;
        while(Result) {
            // načtení poloky ze urnálu
            try {
                Header = Journal_.ReadUInt32();
                Command = (QueryCmd)Journal_.ReadUInt32();
            }
            catch(System.IO.EndOfStreamException) {
                break;
            }

            if(Header == Constants.JournalHeader) {
                switch(Command) {
                case QueryCmd.CMDADDCITY:
                    Result = Db_.AddCity(GetString()) != 0;
                    break;
                case QueryCmd.CMDADDCITYALIAS:
                    {
                        string Name = GetString();
                        uint Id = Journal_.ReadUInt32();
                        Result = Db_.AddCityAlias(Name, Id);
                    }
                    break;
                case QueryCmd.CMDDELETECITY:
                    Result = Db_.DeleteCity(GetString());
                    break;
                case QueryCmd.CMDADDBUSPLAN:
                    {
                        string Name = GetString();
                        uint Seats = Journal_.ReadUInt32();
                        string Data = GetString();
                        Result = Db_.AddBusPlan(Name, Seats, Data) != 0;
                    }
                    break;
                case QueryCmd.CMDDELETEBUSPLAN:
                    Result = Db_.DeleteBusPlan(GetString());
                    break;
                case QueryCmd.CMDADDBUS:
                    {
                        BusInfo bus = new BusInfo();
                        bus.Id = Journal_.ReadUInt32();
                        bus.Line = Journal_.ReadUInt32();
                        bus.CityFrom = Journal_.ReadUInt32();
                        bus.CityTo = Journal_.ReadUInt32();
                        bus.LeaveTime = DateTime.FromBinary(Journal_.ReadInt64());
                        bus.Plan = Journal_.ReadUInt32();
                        Result = Db_.AddBus(bus) != 0;
                    }
                    break;
                case QueryCmd.CMDDELETEBUS:
                    Result = Db_.DeleteBus(Journal_.ReadUInt32());
                    break;
                case QueryCmd.CMDENABLEBUS:
                    Result = Db_.EnableBus(Journal_.ReadUInt32());
                    break;
                case QueryCmd.CMDDISABLEBUS:
                    Result = Db_.DisableBus(Journal_.ReadUInt32());
                    break;
                case QueryCmd.CMDNEWCUSTOMER:
                    Db_.NewCustomer();
                    break;
                case QueryCmd.CMDLOCKSEATS:
                    {
                        uint Bus = Journal_.ReadUInt32();
                        uint Customer = Journal_.ReadUInt32();
                        uint It = Journal_.ReadUInt32();
                        Queue<uint> queue = new Queue<uint>();
                        for(int i = 0; i < It; i++) {
                            queue.Enqueue(Journal_.ReadUInt32());
                        }
                        Result = Db_.LockSeats(Bus, ref queue, Customer);
                    }
                    break;
                case QueryCmd.CMDUNLOCKSEATS:
                    {
                        uint Bus = Journal_.ReadUInt32();
                        uint Customer = Journal_.ReadUInt32();
                        uint It = Journal_.ReadUInt32();
                        Queue<uint> queue = new Queue<uint>();
                        for(int i = 0; i < It; i++) {
                            queue.Enqueue(Journal_.ReadUInt32());
                        }
                        Result = Db_.UnlockSeats(Bus, ref queue, Customer);
                    }
                    break;
                default: Result = false; break; // nepovolený nebo neznámý příkaz
                }
                Items++;
            }
            else
                Result = false;
        }
        Journal_.Close();
        if(!Result)
            System.IO.File.Move(Path_ + JournalName_, Path_ + JournalName_ + "_failed"); // záloha nezpracovaného urnálu
        return Result;
    }
    private string Path_;
    private MemoryDb Db_;
    private const string JournalName_ = "brs.journal";
    private System.IO.BinaryReader Journal_;
    private string GetString() {
        uint Length = Journal_.ReadUInt32();
        byte[] data = new byte[Length];
        data = Journal_.ReadBytes((int)Length);
        return System.Text.Encoding.UTF8.GetString(data);
    }
};

/// <summary>
/// Třída pro zápis do žurnálu.
/// </summary>
/// Třída zajišťuje práci se souborem žurnálu. Slouží k přidávání jednotlivých položek do žurnálu, 
/// zároveň kontroluje, zda velikost souboru nepřesáhla přednastavený limit. Pokud ano, je 
/// probuzeno pracovní vlákno, které zajistí provedení zálohy databáze a vyprázdnění žurnálu.
/// Podrobněji @ref journal "zde".
class WriteJournal {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// Vytvoření třídy obnáší i vytvoření synchronizačním objektů a vytvoření (prázdného) souboru žurnálu v požadovaném 
    /// umístění.
    /// <param name="Path">určení složky pro uložení interní zálohy databáze a souboru žurnálu.
    /// Konkrétní jména jsou určena vnitřními konstantami. Volající je odpovědný za to, že 
    /// proces má odpovídající přístupová práva do zadané složky.</param>
    /// <param name="Limit">limit velikosti žurnálového souboru. Při jeho překročení je vyvolána 
    /// interní záloha databáze.</param>
    /// <param name="Db">ukazatel na instanci databáze, se kterou je žurnál spojen.</param>
    public WriteJournal(string Path, uint Limit, MemoryDb Db) {
        Path_ = Path;
        Db_ = Db;
        Limit_ = Limit;
        Lock_ = new Mutex();
        File_ = new System.IO.BinaryWriter(new System.IO.FileStream(Path_ + JournalName_, System.IO.FileMode.Create));
        Written_ = 0;
    }
    /// <summary>
    /// Požadavek na vyprázdnění žurnálu.
    /// </summary>
    /// Obsluha vnějšího požadavku na provedení interní zálohy celé databáze a tím i vyprázdnění 
    /// souboru žurnálu. Požadavek je uložen a bude vyřízen pracovním vláknem co nejdříve (po 
    /// získání výhradního přístupu k databázi).
    public void AskBackup() {
        t_ = new Thread(InternalBackup);
        t_.Start();
    }
    /// <summary>
    /// Zapsání příkazu do žurnálu.
    /// </summary>
    /// <param name="cmd">Příkaz k zapsání.</param>
    public void Write(QueryCmd cmd) {
        lock(Lock_) {
            AddItem(cmd);
            File_.Flush();
            BackupIfFull();
        }
    }
    /// <summary>
    /// Zapsání příkazu do žurnálu.
    /// </summary>
    /// <param name="cmd">Příkaz k zapsání.</param>
    /// <param name="text">Textová parametr.</param>
    public void Write(QueryCmd cmd, string text) {
        lock(Lock_) {
            AddItem(cmd);
            AddItem(text);
            File_.Flush();
            BackupIfFull();
        }
    }
    /// <summary>
    /// Zapsání příkazu do žurnálu.
    /// </summary>
    /// <param name="cmd">Příkaz k zapsání.</param>
    /// <param name="text">Textový parametr.</param>
    /// <param name="number">Neznaménkové číslo.</param>
    public void Write(QueryCmd cmd, string text, uint number) {
        lock(Lock_) {
            AddItem(cmd);
            AddItem(text);
            AddItem(number);
            File_.Flush();
            BackupIfFull();
        }
    }
    /// <summary>
    /// Zapsání příkazu do žurnálu.
    /// </summary>
    /// <param name="cmd">Příkaz k zapsání.</param>
    /// <param name="text">Textový parametr.</param>
    /// <param name="number">Neznaménkové číslo.</param>
    /// <param name="data">Textový parametr.</param>
    public void Write(QueryCmd cmd, string text, uint number, string data) {
        lock(Lock_) {
            AddItem(cmd);
            AddItem(text);
            AddItem(number);
            AddItem(data);
            File_.Flush();
            BackupIfFull();
        }
    }
    /// <summary>
    /// Zapsání příkazu do žurnálu.
    /// </summary>
    /// <param name="cmd">Příkaz k zapsání.</param>
    /// <param name="info">Info.</param>
    public void Write(QueryCmd cmd, BusInfo info) {
        lock(Lock_) {
            AddItem(cmd);
            AddItem(info.Id);
            AddItem(info.Line);
            AddItem(info.CityFrom);
            AddItem(info.CityTo);
            AddItem(info.LeaveTime);
            AddItem(info.Plan);
            File_.Flush();
            BackupIfFull();
        }
    }
    /// <summary>
    /// Zapsání příkazu do žurnálu.
    /// </summary>
    /// <param name="cmd">Příkaz k zapsání.</param>
    /// <param name="number">Neznaménkové číslo.</param>
    public void Write(QueryCmd cmd, uint number) {
        lock(Lock_) {
            AddItem(cmd);
            AddItem(number);
            File_.Flush();
            BackupIfFull();
        }
    }
    /// <summary>
    /// Zapsání příkazu do žurnálu.
    /// </summary>
    /// <param name="cmd">Příkaz k zapsání.</param>
    /// <param name="num1">Neznaménkové číslo.</param>
    /// <param name="num2">Neznaménkové číslo.</param>
    /// <param name="queue">Fronta neznaménkových čísel.</param>
    public void Write(QueryCmd cmd, uint num1, uint num2, Queue<uint> queue) {
        lock(Lock_) {
            AddItem(cmd);
            AddItem(num1);
            AddItem(num2);
            AddItem((uint)queue.Count);
            foreach(var num in queue) {
                AddItem(num);
            }
            File_.Flush();
            BackupIfFull();
        }
    }
    private bool AddItem(QueryCmd command) {
        File_.Write(Constants.JournalHeader);
        File_.Write((uint)command);
        Written_++;
        return true;
    }
    private bool AddItem(uint number) {
        File_.Write(number);
        Written_++;
        return true;
    }
    private bool AddItem(string text) {
        byte[] str = System.Text.Encoding.UTF8.GetBytes(text);
        uint length = (uint)str.Length;
        File_.Write(length);
        File_.Write(str);
        Written_++;
        return true;
    }
    private bool AddItem(DateTime time) {

        File_.Write(time.ToBinary());
        Written_++;
        return true;
    }
    /// <summary>
    /// Provedení zálohy, vyprázdnění a uzavření žurnálu.
    /// </summary>
    public void OnStop() {
        AskBackup();
        t_.Join();
        File_.Close();
        System.IO.File.Delete(Path_ + JournalName_);
    }
    private void InternalBackup() {
        if(Db_ != null && Written_ > 0) {
            Db_.LockDatabase_.EnterWriteLock(); // zámek celé databáze - výlučný přístup
            lock(Lock_) { // zajitění serializace poadavků
                File_.Close();
                // provedení zálohy databáze
                if(System.IO.File.Exists(Path_ + DbName_)) {
                    System.IO.File.Move(Path_ + DbName_, Path_ + TempDbName_); // dočasná záloha
                }
                System.IO.FileStream Backup = new System.IO.FileStream(Path_ + DbName_, System.IO.FileMode.Create);
                bool Done = Db_.BackupNoLock(Backup); // zajiuje výhradní přístup k databázi
                Backup.Close();
                if(Done) {
                    Written_ = 0;
                    System.IO.File.Delete(Path_ + TempDbName_); // vymazání dočasné zálohy
                    // vymazání (znovuotevření) urnálu
                    File_ = new System.IO.BinaryWriter(new System.IO.FileStream(Path_ + JournalName_, System.IO.FileMode.Create));
                }
            }
            Db_.LockDatabase_.ExitWriteLock(); // zámek celé databáze
        }
    }
    private void BackupIfFull() {
        if(Written_ >= Limit_) {
            AskBackup();
        }
    }
    private string Path_;
    private uint Limit_;
    private MemoryDb Db_;
    private System.IO.BinaryWriter File_;
    private Mutex Lock_;
    private Thread t_;
    private uint Written_;
    private const string DbName_ = "brs.database";
    private const string TempDbName_ = "brs.tmp";
    private const string JournalName_ = "brs.journal";
};