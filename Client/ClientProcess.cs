﻿using System;
using System.Collections.Generic;


/// <summary>
/// Klientská třída zajišťující zpřístupnění vzdálené databáze ze serveru.
/// </summary>
/// Díky tomy, že implementuje rozhraní IQuery, tak lze tuto třídu nahradit novou implementací,
/// která bude například používat jiný způsob komunikace. Nebo lze přímo připojit i samotnou databázi
/// MemoryDb, což může mít dobrý význam zvlášť při ladění chyb.
class ClientProcess : IQuery {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="server">Doménové jméno nebo IP adresa serveru.</param>
    /// <param name="port">Port serveru.</param>
    public ClientProcess(string server, int port) {
        connection_ = new ClientConnection(server, port);
    }
    public uint AddCity(string Name) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDADDCITY);
        connection_.Write(Name);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool AddCityAlias(string Name, uint Id) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDADDCITYALIAS);
        connection_.Write(Name);
        connection_.Write(Id);
        bool result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool DeleteCity(string Name) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDDELETECITY);
        connection_.Write(Name);
        bool result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public uint FindCity(string Name) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDFINDCITY);
        connection_.Write(Name);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool CityInfo(uint City, out CityInfo Info, out uint LinesOut, out uint LinesIn) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDCITYINFO);
        connection_.Write(City);
        bool result;
        connection_.Read(out result);
        Info = new global::CityInfo();
        if(result) {
            connection_.Read(out Info.Id);
            connection_.Read(out Info.Name);
            connection_.Read(out LinesOut);
            connection_.Read(out LinesIn);
        }
        else {
            LinesOut = 0;
            LinesIn = 0;
        }
        connection_.DisconnectServer();
        return result;
    }
    public uint AddBusPlan(string Name, uint Seats, string Data) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDADDBUSPLAN);
        connection_.Write(Name);
        connection_.Write(Seats);
        connection_.Write(Data);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool DeleteBusPlan(string Name) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDDELETEBUSPLAN);
        connection_.Write(Name);
        bool result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public uint FindBusPlan(string Name) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDFINDBUSPLAN);
        connection_.Write(Name);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool BusPlanInfo(uint Plan, out PlanInfo Info, out uint Buses, out string Data) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDBUSPLANINFO);
        connection_.Write(Plan);
        bool result;
        connection_.Read(out result);
        Info = new global::PlanInfo();
        if(result) {
            connection_.Read(out Info.Id);
            connection_.Read(out Info.Name);
            connection_.Read(out Info.Seats);
            connection_.Read(out Buses);
            connection_.Read(out Data);
        }
        else {
            Buses = 0;
            Data = null;
        }
        connection_.DisconnectServer();
        return result;
    }
    public uint AddBus(BusInfo Bus) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDADDBUS);
        connection_.Write(Bus.Id);
        connection_.Write(Bus.Line);
        connection_.Write(Bus.CityFrom);
        connection_.Write(Bus.CityTo);
        connection_.Write(Bus.LeaveTime);
        connection_.Write(Bus.Plan);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool DeleteBus(uint Bus) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDDELETEBUS);
        connection_.Write(Bus);
        bool result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public uint FindBus(uint From, uint To, DateTime LeaveTime) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDFINDBUS);
        connection_.Write(From);
        connection_.Write(To);
        connection_.Write(LeaveTime);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool BusInfo(uint Bus, out BusInfo Info, out bool Enabled, out uint Seats, out Queue<uint> FreeSeats) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDBUSINFO);
        connection_.Write(Bus);
        bool result;
        connection_.Read(out result);
        FreeSeats = new Queue<uint>();
        Info = new global::BusInfo();
        if(result) {
            connection_.Read(out Info.Id);
            connection_.Read(out Info.Line);
            connection_.Read(out Info.CityFrom);
            connection_.Read(out Info.CityTo);
            connection_.Read(out Info.LeaveTime);
            connection_.Read(out Info.Plan);
            connection_.Read(out Enabled);
            connection_.Read(out Seats);
            uint free_seats;
            connection_.Read(out free_seats);
            for(uint i = 0; i < free_seats; i++) {
                uint seat;
                connection_.Read(out seat);
                FreeSeats.Enqueue(seat);
            }
        }
        else {
            Enabled = true;
            Seats = 0;
        }
        connection_.DisconnectServer();
        return result;
    }
    public bool EnableBus(uint Bus) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDENABLEBUS);
        connection_.Write(Bus);
        bool result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool DisableBus(uint Bus) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDDISABLEBUS);
        connection_.Write(Bus);
        bool result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public uint NewCustomer() {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDNEWCUSTOMER);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool AskSeats(uint Bus, ref Queue<uint> Seats, uint Customer) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDASKSEATS);
        connection_.Write(Bus);
        connection_.Write(Customer);
        uint count = (uint)Seats.Count;
        connection_.Write(count);
        for(uint i = 0; i < count; i++) {
            connection_.Write(Seats.Dequeue());
        }
        bool result;
        connection_.Read(out result);
        Seats.Clear();
        uint recv;
        connection_.Read(out recv);
        for(uint i = 0; i < recv; i++) {
            uint item;
            connection_.Read(out item);
            Seats.Enqueue(item);
        }
        connection_.DisconnectServer();
        return result;
    }
    public bool LockSeats(uint Bus, ref Queue<uint> Seats, uint Customer) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDLOCKSEATS);
        connection_.Write(Bus);
        connection_.Write(Customer);
        uint count = (uint)Seats.Count;
        connection_.Write(count);
        for(uint i = 0; i < count; i++) {
            connection_.Write(Seats.Dequeue());
        }
        bool result;
        connection_.Read(out result);
        Seats.Clear();
        uint recv;
        connection_.Read(out recv);
        for(uint i = 0; i < recv; i++) {
            uint item;
            connection_.Read(out item);
            Seats.Enqueue(item);
        }
        connection_.DisconnectServer();
        return result;
    }
    public bool UnlockSeats(uint Bus, ref Queue<uint> Seats, uint Customer) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDUNLOCKSEATS);
        connection_.Write(Bus);
        connection_.Write(Customer);
        uint count = (uint)Seats.Count;
        connection_.Write(count);
        for(uint i = 0; i < count; i++) {
            connection_.Write(Seats.Dequeue());
        }
        bool result;
        connection_.Read(out result);
        Seats.Clear();
        uint recv;
        connection_.Read(out recv);
        for(uint i = 0; i < recv; i++) {
            uint item;
            connection_.Read(out item);
            Seats.Enqueue(item);
        }
        connection_.DisconnectServer();
        return result;
    }
    public uint NextBus(uint Bus) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDNEXTBUS);
        connection_.Write(Bus);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public uint NextPlan(uint Plan) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDNEXTPLAN);
        connection_.Write(Plan);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public uint NextCity(uint City) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDNEXTCITY);
        connection_.Write(City);
        uint result;
        connection_.Read(out result);
        connection_.DisconnectServer();
        return result;
    }
    public bool ListCity(string From, out string Name) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDLISTCITY);
        connection_.Write(From);
        bool result;
        connection_.Read(out result);
        if(result) {
            connection_.Read(out Name);
        }
        else {
            Name = null;
        }
        connection_.DisconnectServer();
        return result;
    }
    public bool ListAlias(string From, out string Alias, out string City) {
        connection_.ConnectServer();
        connection_.Write(QueryCmd.CMDLISTALIAS);
        connection_.Write(From);
        bool result;
        connection_.Read(out result);
        if(result) {
            connection_.Read(out Alias);
            connection_.Read(out City);
        }
        else {
            Alias = null;
            City = null;
        }
        connection_.DisconnectServer();
        return result;
    }
    private ClientConnection connection_;
}
