using System;
using System.Collections.Generic;
using System.Threading;
using C5;

/// <summary>
/// lán autobusu.
/// </summary>
/// Třída si kromě konstantních položek (@ref PlanInfo) uchovává i počet autobusů, které daný plán používají.
/// Plán autobusu má textovou podobu.
class Plan {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// <param name="Info">struktura @ref PlanInfo se základními údaji.</param>
    /// <param name="data">Textový plánek s rozvržením sedadel.</param>
    public Plan(PlanInfo Info, string data) {
        PlanInfo_ = Info;
        Data_ = data;
        Lock_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        Buses_ = 0;
    }
    /// <summary>
    /// Načtení z proudu.
    /// </summary>
    /// Třída je vytvořena načtením všech položek z proudu.
    /// <param name="stream">vstupní proud.</param>
    public Plan(System.IO.Stream stream) {
        PlanInfo_ = new PlanInfo();
        Restore restore = new Restore(stream);
        restore.Header(BkHeader.DatObject.Plan);
        PlanInfo_.Id = restore.Unsigned();
        PlanInfo_.Name = restore.String();
        PlanInfo_.Seats = restore.Unsigned();
        Buses_ = restore.Unsigned();
        Data_ = restore.String();
        Lock_ = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
    }
    /// <summary>
    /// Získání informací o plánu.
    /// </summary>
    /// Vyplnění struktury @ref PlanInfo.
    /// <param name="Info">struktura s daty.</param>
    public void GetInfo(out PlanInfo Info) {
        Info = PlanInfo_;
    }
    /// <summary>
    /// Vrátí ID plánu.
    /// </summary>
    public uint Id() {
        return PlanInfo_.Id;
    }
    /// <summary>
    /// Vrátí název plánu jako konstantní řetězec.
    /// </summary>
    public string Name() {
        return PlanInfo_.Name;
    }
    /// <summary>
    /// Vrátí počet sedadel.
    /// </summary>
    public uint Seats() {
        return PlanInfo_.Seats;
    }
    /// <summary>
    /// Vrátí počet autobusů používajících tento plán.
    /// </summary>
    public uint Buses() {
        uint result;
        Lock_.EnterReadLock();
        result = Buses_;
        Lock_.ExitReadLock();
        return result;
    }
    /// <summary>
    /// Změna čítače autobusů používajících tento plán o hodnotu @a Change.
    /// </summary>
    /// <param name="Change">hodnota, o kterou se změní počet autobusů, které používají tento plán.</param>
    public void Buses(int Change) {
        Lock_.EnterWriteLock();
        if(Change < 0) {
            Buses_ -= (uint)Math.Abs(Change);
        }
        else {
            Buses_ += (uint)Change;
        }
        Lock_.ExitWriteLock();
    }
    /// <summary>
    /// Získání textových informací plánu.
    /// </summary>
    /// <param name="data">textové informace plánu.</param>
    public void Data(out string data) {
        data = Data_;
    }
    /// <summary>
    /// Záloha.
    /// </summary>
    /// Vytvoření zálohy třídy všech datových položek do proudu (souboru, soketu, ...).
    /// <param name="stream">výstupní proud.</param>
    public void Backup(System.IO.Stream stream) {
        Backup backup = new global::Backup(stream);
        backup.Header(BkHeader.DatObject.Plan);
        backup.Unsigned(PlanInfo_.Id);
        backup.String(PlanInfo_.Name);
        backup.Unsigned(PlanInfo_.Seats);
        backup.Unsigned(Buses_);
        backup.String(Data_);
    }
    private uint Buses_;
    private PlanInfo PlanInfo_;
    private string Data_;
    private ReaderWriterLockSlim Lock_;
};
    


/// <summary>
/// Kontajner plánů.
/// </summary>
/// Třída sdružuje všechny plány autobusů. Stará se o správné přidávání, mazání a hledání plánů,
/// díky @ref AutoIncrement i o jejich jedinečné číslování. Plány jsou uloženy v asociativním 
/// kontajneru, řazení je provedeno dle unikátního čísla.
class Plans {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    /// Vytvoření prázdného kontajneru.
    public Plans() {
        PlanSet_ = new TreeDictionary<uint, Plan>();
        Increment_ = new AutoIncrement();
    }
    /// <summary>
    /// Načtení z proudu.
    /// </summary>
    /// Kontajner je vytvořen načtením všech položek z proudu.
    /// <param name="stream">vstupní proud.</param>
    public Plans(System.IO.Stream stream) : this() {
        Restore restore = new Restore(stream);
        restore.Header(BkHeader.DatObject.Plans);
        Increment_.Set(restore.Unsigned());
        uint items = restore.Unsigned();
        for(uint i = 0; i < items; i++) {
            var temp = new Plan(stream);
            PlanSet_.Add(temp.Id(), temp);
        }
    }
    /// <summary>
    /// Přidání nového plánu.
    /// </summary>
    /// Získání unikátního ID pro plán pomocí @ref AutoIncrement a vložení do kontejneru.
    /// <param name="Name">název plánu nebo typu autobusu.</param>
    /// <param name="Seats">počet sedadel autobusu.</param>
    /// <param name="data">textová data rozvržení sedadel.</param>
    /// <returns>nově vytvořený plán.</returns>
    public Plan Add(string Name, uint Seats, string data) {
        PlanInfo info = new PlanInfo();

        info.Id = Increment_.Next();
        info.Name = Name;
        info.Seats = Seats;
        Plan plan = new Plan(info, data);
        PlanSet_.Add(plan.Id(), plan);
        return plan;
    }
    /// <summary>
    /// Smazání plánu.
    /// </summary>
    /// <param name="Id">unikátní ID plánu určeného ke smazání.</param>
    /// <returns>@e true při úspěchu, @e false jinak</returns>
    public bool Delete(uint Id) {
        return PlanSet_.Remove(Id);
    }
    /// <summary>
    /// Nalezení plánu podle ID.
    /// </summary>
    /// <param name="Id">ID hledaného plánu.</param>
    /// <returns>nalezený plán nebo @e null</returns>
    public Plan Find(uint Id) {
        Plan plan;
        if(PlanSet_.Find(ref Id, out plan)) {
            return plan;
        }
        else {
            return null;
        }
    }
    /// <summary>
    /// Nalezení dalšího plánu v pořadí.
    /// </summary>
    /// Nalezne a vrátí ID dalšího plánu v pořadí po @e Id. Pokud takový neexistuje, vrátí @e 0.
    public uint GetNextId(uint Id) {
        C5.KeyValuePair<uint, Plan> temp;
        if(PlanSet_.TrySuccessor(Id, out temp)) {
            return temp.Key;
        }
        else {
            return 0;
        }
    }
    /// <summary>
    /// Zjištění počtu.
    /// </summary>
    /// Zjištění počtu objektů v kontajneru.
    /// <returns>aktuální počet.</returns>
    public uint Count() {
        return (uint)PlanSet_.Count;
    }
    /// <summary>
    /// Vymazání kontajneru.
    /// </summary>
    /// Vymazání všech položek kontajneru.
    /// @note Vymazány jsou pouze prvky kontajneru, pokud k němu existuje nějaký index, je nutné
    /// jeho vymazání externími prostředky.
    public void Clear() {
        PlanSet_.Clear();
    }
    /// <summary>
    /// Záloha.
    /// </summary>
    /// Vytvoření zálohy kontajneru včetně dalších datových položek do proudu (souboru, soketu, ...).
    /// <param name="stream">výstupní proud.</param>
    public void Backup(System.IO.Stream stream) {
        Backup backup = new global::Backup(stream);
        backup.Header(BkHeader.DatObject.Plans);
        backup.Unsigned(Increment_.Next());
        backup.Unsigned((uint)PlanSet_.Count);
        foreach(var plan in PlanSet_) {
            plan.Value.Backup(stream);
        }
    }
    private TreeDictionary<uint, Plan> PlanSet_;
    private AutoIncrement Increment_;
};