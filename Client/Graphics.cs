﻿using System;
using System.Collections.Generic;

/// <summary>
/// Třída zajišťující grafický interface (vstup a výstup).
/// </summary>
/// Třída slouží k získávání vstupu od uživatele a sdělování zpátky výsledku. Všechny informace jsou sdělovány přes standardní vstup a výstup.
class Graphics {
    /// <summary>
    /// Konstruktor.
    /// </summary>
    public Graphics() {}
    /// <summary>
    /// Přečtení řádky ze vstupu.
    /// </summary>
    /// Metoda přečte ze vstupu řádek a uloží ho do připraveného bufferu. Z řádky odstraní případný znak newline '\\n'.
    /// <param name="Buffer">buffer k naplnění řetězcem zadaným uživatelem.</param>
    /// <returns>@e true při úspěchu, @e false jinak (např. při dosažení konce souboru v případě, kdy je soubor přesměrován na standardní vstup)</returns>
    public bool Read(out string Buffer) {
        Console.Write("> ");
        try {
            Buffer = Console.ReadLine();
        }
        catch(System.IO.IOException) {
            Buffer = null;
            return false;
        }
        if(Buffer == null) {
            return false;
        }
        return true;
    }
    /// <summary>
    /// Zapsání řetězce na výstup.
    /// </summary>
    /// <param name="Buffer">řetězec, který bude vypsán na standardní výstup.</param>
    public void Write(string Buffer) {
        Console.WriteLine(Buffer);
    }
    /// <summary>
    /// Vypsání autobusů.
    /// </summary>
    /// Vypsání autobusů uložených v kontejneru @e Queue, používá se pro výpis po příkazech "find" a "next".
    /// <param name="Buses">fronta struktur @ref BusPrint obsahující informace o vypisovaných autobusech.</param>
    public void WriteBuses(Queue<BusPrint> Buses) {
        WriteBusHeader();
        foreach(var bus in Buses) {
            WriteBusItem(bus);
        }
    }
    /// <summary>
    /// Vypsání sedadel.
    /// </summary>
    /// Vypsání sedadel v čitelné formě (pokud je to možné, tak rozsahem - např. místo <tt>5 6 7 8 11 12</tt> je uvedeno <tt>5-8 11-12</tt>) spolu s jejich počtem.
    /// <param name="Prefix">text uvedený před seznamem sedadel.</param>
    /// <param name="Seats">seznam sedadel (předpokládá se, že seznam je vzestupně setříděný a neobsahuje duplicitní záznamy).</param>
    public void WriteSeats(string Prefix, Queue<uint> Seats) {
        Console.Write(Prefix);
        if(Seats.Count == 0) {
            Console.Write("---");
        }
        else {
            var copy = Seats.GetEnumerator();
            copy.MoveNext();
            uint Last = copy.Current;
            Console.Write(Last);
            bool Range = false;
            bool NotEnd = copy.MoveNext();
            while(NotEnd) {
                if(copy.Current == Last + 1) {
                    Last++;
                    Range = true;
                }
                else {
                    if(Range) {
                        Console.Write("-{0}", Last);
                    }
                    Last = copy.Current;
                    Console.Write(" {0}", Last);
                    Range = false;
                }
                NotEnd = copy.MoveNext();
            }
            if(Range) {
                Console.Write("-{0}", Last);
            }
            Console.Write(" (total: {0})", Seats.Count);
        }
        Console.WriteLine();
    }
    /// <summary>
    /// Vypsání plánu autobusu.
    /// </summary>
    /// <param name="Plan">textová podoba plánu autobusu.</param>
    /// <param name="FreeSeats">seznam volných sedadel.</param>
    /// <param name="BusData">informace o autobusu.</param>
    /// <param name="NumberSeats">celkový počet sedadel autobusu.</param>
    /// <param name="Prefix">řetězec vytištěný před výpis sedadel.</param>
    public void WriteBusPlan(string Plan, Queue<uint> FreeSeats, BusPrint BusData, uint NumberSeats, string Prefix = "Free seats: ") {
        WriteBusHeader();
        WriteBusItem(BusData);
        Console.Write("Bus plan: ");
        var j = FreeSeats.GetEnumerator();
        var PlanBuilder = new System.Text.StringBuilder(Plan);
        bool End = !j.MoveNext(); // posun na prvni prvek
        for(uint i = 1; i <= NumberSeats; i++) {
            if(End || j.Current > i) {
                string Number = " " + i.ToString() + " ";
                string New = " ";
                for(uint k = 0; k < Number.Length - 2; k++) {
                    New += "-";
                }
                New += " ";
                PlanBuilder.Replace(Number, New);
            }
            else if(j.Current < i) {
                while(j.Current <= i) {
                    End = !j.MoveNext();
                }
            }
            else {
                End = !j.MoveNext();
            }
        }
        Write(PlanBuilder.ToString());
        WriteSeats(Prefix, FreeSeats);
    }
    /// <summary>
    /// Vypsání jízdenky po úspěšné rezervaci.
    /// </summary>
    /// Po úspěšné rezervaci sedadel je uživateli vystaveno potvrzení se všemi potřebnými údaji
    /// (včetně čísla zakazníka). Jejich pomocí může následně provést i zrušení rezervace.
    /// <param name="Id">ID zákazníka.</param>
    /// <param name="Seats">seznam zarezervovaných sedadel.</param>
    /// <param name="BusPrint">informace o autobusu.</param>
    public void WriteReservation(uint Id, Queue<uint> Seats, BusPrint BusPrint) {
        Console.Write("\nYour ticket:\n\n");
        Console.WriteLine("{0,-9}{1,-8}{2,-20}{3,-20}{4,-20}", "USER ID", "BUS ID", "CITY FROM", "CITY TO", "LEAVE TIME");
        Console.WriteLine("{0,-9}{1,-8}{2,-20}{3,-20}{4,2:D2}:{5,2:D2}-{6,2:D2}.{7,2:D2}.{8,-8}", Id, BusPrint.Id, BusPrint.CityFrom, BusPrint.CityTo, BusPrint.LeaveTime.Hour,
            BusPrint.LeaveTime.Minute, BusPrint.LeaveTime.Day, BusPrint.LeaveTime.Month, BusPrint.LeaveTime.Year);
        WriteSeats("Reserved seats: ", Seats);
    }
    /// <summary>
    /// Vypsání ID nově přidaného města.
    /// </summary>
    /// <param name="Id">ID nově přidaného města.</param>
    public void WriteAddCity(uint Id) {
        Console.WriteLine("OK. City number: {0}", Id);
    }
    /// <summary>
    /// Vypsání ID nově přidaného autobusu.
    /// </summary>
    /// <param name="Id">ID nově přidaného autobusu.</param>
    public void WriteAddBus(uint Id) {
        Console.WriteLine("OK. Bus number: {0}", Id);
    }
    /// <summary>
    /// Vypsání ID nově přidaného plánu autobusu.
    /// </summary>
    /// <param name="Id">ID nově přidaného plánu autobusu.</param>
    public void WriteAddPlan(uint Id) {
        Console.WriteLine("OK. Plan number: {0}", Id);
    }
    /// <summary>
    /// Potvrzení volby.
    /// </summary>
    /// Metoda vypíše výzvu k potvrzení, následně čeká na odpověď (vstup zůstane po konci funkce prázdný).
    /// <returns>@e true v případě potvrzení uživatelem, jinak @e false</returns>
    public bool Continue() {
        Console.Write("Write \'y\' to continue: ");
        string c = Console.ReadLine();
        if(c == null || c != "y") {
            return false;
        }
        else {
            return true;
        }
    }
    /// <summary>
    /// Vypsání nápovědy.
    /// </summary>
    /// <param name="FullClient">Přepínač, zda je tento klient v plném nebo omezeném módu.</param>
    public void WriteHelp(bool FullClient) {
        Console.WriteLine("List of available commands:");
        Console.WriteLine("EXIT\tcloses the program");
        Console.WriteLine("FIND\tfinds 3 buses\n\tExamples:\tfind Sobeslav Praha 12:20\n\t\t\tfind \"Hradec Kralove\" Praha 15:18-2.10.2013");
        Console.WriteLine("NEXT\tfinds next 3 buses - use FIND first");
        Console.WriteLine("BUS\tsets active bus by number\n\tExample: bus 15");
        Console.WriteLine("RESERVE\treserves seats in current bus\n\tExamples:\treserve 4,5,6\t- reserves seats 4, 5, 6\n\t\t\treserve #4\t- reserves 4 first seats");
        Console.WriteLine("UNLOCK\tunlocks reserved seats in current bus\n\tExamples:\tunlock 2\t- unlocks seats with customer number 2\n\t\t\tunlock 2 4,5,6\t- unlock seats 4, 5, 6 with customer number 2");
        if(FullClient) {
            Console.WriteLine("ADDBUS\tadds new bus\n\tExample:\taddbus 12 Sobeslav Praha 15:18-2.10.2013 Karosa652\n\t\t- adds bus from Sobeslav to Praha at 15:18-2.10.2013\n\t\t  with line number 12 and bus plan Karosa652");
            Console.WriteLine("DELBUS\tdeletes bus with specified number\n\tExample: delbus 5");
            Console.WriteLine("ADDCITY\tadds new city\n\tExample: addcity Kolin");
            Console.WriteLine("DELCITY\tdeletes city\n\tExample: delcity Kolin");
            Console.WriteLine("ADDCITYALIAS\tadds new alias to existing city\n\tExample: addcityalias HK \"Hradec Kralove\"");
            Console.WriteLine("ADDBUSPLAN\tadds new bus plan\n\tExample: addbusplan Karosa652 42 karosa652.txt\n\t\t- adds bus plan with name Karosa652 with 42 seats\n\t\t  and text sketch of plan is in file karosa652.txt");
            Console.WriteLine("DELBUSPLAN\tdeletes bus plan\n\tExample: delbusplan Karosa652");
            Console.WriteLine("CLEAR\tclears old buses/unused cities/unused bus plans\n\tExamples:\tclear bus\n\t\t\tclear city\n\t\t\tclear plan");
            Console.WriteLine("CLOSE\tcloses bus for reservations\n\tExample:\tclose 5\t\t- closes bus ID 5");
        }
        Console.WriteLine("CITY\tprints existing cities, optional with given prefix\n\tExample:\tcity\t\t- prints all cities and aliases\n\t\t\tcity Pr\t\t- prints cities and aliases with prefix \'Pr\'");
        Console.WriteLine("HELP\tprints this help page");
        Console.WriteLine("\nCommands are case insensitive.");
    }
    /// <summary>
    /// Vypsání hlavičky při spuštění.
    /// </summary>
    public void WriteWelcome() {
        Console.WriteLine("Bus Reservation System 1.0\nPetr Stefan - Programming in C#, 2014/2015");
    }
    /// <summary>
    /// Vypsání počtu smazaných položek.
    /// </summary>
    /// <param name="Count">počet smazaných položek.</param>
    public void WriteDeleted(uint Count) {
        if(Count == 0) {
            Console.WriteLine("OK. No items deleted.");
        }
        else {
            Console.WriteLine("OK. Items deleted: {0}", Count);
        }
    }
    /// <summary>
    /// Vypasání existujích měst.
    /// </summary>
    /// <param name="Cities">seznam měst k vypsání. Název města je v prvním stringu. Pokud nalezený
    /// výsledek je alias, je uložen v druhém stringu (pokud je druhý string neprázdný, vypisuje
    /// se alias a za ním město).</param>
    public void WriteCities(List<KeyValuePair<string, string>> Cities) {
        foreach(var it in Cities) {
            if(it.Value == "") {
                Console.WriteLine(it.Key);
            }
            else {
                Console.WriteLine("{0} -> {1}", it.Key, it.Value);
            }
        }
    }
    private void WriteBusHeader() {
        Console.WriteLine("{0,-5}{1,-5}{2,-20}{3,-20}{4,-17}{5,-12}", "ID", "LINE", "FROM", "TO", "TIME", "PLAN");
    }
    private void WriteBusItem(BusPrint Bus) {
        Console.WriteLine("{0,-5}{1,-5}{2,-20}{3,-20}{4,2:D2}:{5,2:D2}-{6,2:D2}.{7,2:D2}.{8,-5}{9,-12}", Bus.Id, Bus.Line, Bus.CityFrom, Bus.CityTo,
            Bus.LeaveTime.Hour, Bus.LeaveTime.Minute, Bus.LeaveTime.Day, Bus.LeaveTime.Month, Bus.LeaveTime.Year, Bus.BusPlan);
    }
}
