﻿using System;

namespace Client
{
    /// <summary>
    /// Hlavní třída klientské aplikace.
    /// </summary>
    class MainClass
    {
        /// <summary>
        /// Vstupní bod programu. Načte konfigurační soubor, vyrobí třídu spojení <see cref="ClientProcess"/> a třídy <see cref="ClientApp"/>, která řídí chod klientské aplikace.
        /// </summary>
        public static void Main(string[] args) {
            string executable = System.Reflection.Assembly.GetEntryAssembly().Location;
            string path;
            if(IsLinux) {
                path = executable.Substring(0, executable.LastIndexOf('/')) + '/';
            }
            else {
                path = executable.Substring(0, executable.LastIndexOf('\\')) + '\\';
            }
            string conf = path + "brs_client.conf";

            int port;
            string host;
            bool isFull;
            if(!ReadConf(conf, out host, out port, out isFull)) {
                Console.WriteLine("Processing config file 'brs_client.conf' failed. ABORTING.");
                return;
            }
            ClientProcess db = new ClientProcess(host, port);
            ClientApp app = new ClientApp(db, isFull);
            app.Run();
        }
        private static bool ReadConf(string conf, out string host, out int port, out bool isFull) {
            bool result = true;
            System.IO.FileStream stream;
            try {
                stream = new System.IO.FileStream(conf, System.IO.FileMode.Open);
            }
            catch(Exception) {
                stream = null;
            }
            host = null;
            port = 0;
            isFull = false;
            if(stream != null) {
                string line;
                System.IO.StreamReader reader = new System.IO.StreamReader(stream);
                while((line = reader.ReadLine()) != null && result) {
                    if(line == "") {
                        continue;
                    }
                    else if(line[0] == '#') {
                        continue;
                    }
                    else if(line.Contains("host")) {
                        string[] splited = line.Split('=');
                        if(splited.Length != 2 || splited[0] != "host") {
                            Console.WriteLine("brs_client.conf - wrong 'host' line.");
                            result = false;
                        }
                        else {
                            host = splited[1];
                        }
                    }
                    else if(line.Contains("port")) {
                        string[] splited = line.Split('=');
                        if(splited.Length != 2 || splited[0] != "port") {
                            Console.WriteLine("brs_client.conf - wrong 'port' line.");
                            result = false;
                        }
                        else {
                            int temp;
                            if(Int32.TryParse(splited[1], out temp)) {
                                port = temp;
                            }
                            else {
                                Console.WriteLine("brs_client.conf - 'port' - invalid number.");
                                result = false;
                            }
                        }
                    }
                    else if(line.Contains("full")) {
                        string[] splited = line.Split('=');
                        if(splited.Length != 2 || splited[0] != "full") {
                            Console.WriteLine("brs_client.conf - wrong 'full' line.");
                            result = false;
                        }
                        else {
                            switch(splited[1]) {
                            case "true":
                                isFull = true;
                                break;
                            case "false":
                                isFull = false;
                                break;
                            default:
                                Console.WriteLine("brs_client.conf - invalid 'full' option.");
                                return false;
                            }
                        }
                    }
                    else {
                        Console.WriteLine("Wrong format of 'brs_client.conf' file.");
                        result = false;
                    }
                }
                reader.Close();
                if(port == 0 || host == null) {
                    Console.WriteLine("brs_client.conf - not all required data provided.\n\tCheck documentation or example config file.");
                    return false;
                }
            }
            else {
                Console.WriteLine("Cannot open 'brs_client.conf' file.");
                result = false;
            }
            return result;
        }
        private static bool IsLinux
        {
            get
            {
                int p = (int)Environment.OSVersion.Platform;
                return (p == 4) || (p == 6) || (p == 128);
            }
        }
    }
}
